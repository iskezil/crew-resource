<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Role_user;

class CreateRoles extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
          'name' => 'Administrator',
          'display_name' => 'Администрация',
          'description' => 'Права боженьки',
        ]);
        Role::create([
          'name' => 'User',
          'display_name' => 'Пользователь',
          'description' => 'Сын божий',
        ]);

        Role_user::create([
          'user_id' => '1',
          'role_id' => '1',
        ]);
    }
}
