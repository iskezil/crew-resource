<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Permission_role;

class CreatePermissions extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
          'name' => 'view-users',
          'display_name' => 'Просмотр списка пользователей',
          'description' => 'Позволяет просматривать всех пользователей',
        ]);

        Permission_role::create([
          'permission_id' => '1',
          'role_id' => '1',
        ]);

        Permission::create([
          'name' => 'update-users',
          'display_name' => 'Редактирование пользователей',
          'description' => 'Позволяет редактировать всех пользователей',
        ]);

        Permission_role::create([
          'permission_id' => '2',
          'role_id' => '1',
        ]);

        Permission::create([
          'name' => 'delete-users',
          'display_name' => 'Удаление пользователей',
          'description' => 'Позволяет удалять пользователей',
        ]);

        Permission_role::create([
          'permission_id' => '3',
          'role_id' => '1',
        ]);

        Permission::create([
          'name' => 'view-roles',
          'display_name' => 'Просмотр списка ролей',
          'description' => 'Позволяет просматривать все роли',
        ]);

        Permission_role::create([
          'permission_id' => '4',
          'role_id' => '1',
        ]);

        Permission::create([
          'name' => 'create-roles',
          'display_name' => 'Добавление новых ролей',
          'description' => 'Позволяет создавать новые роли',
        ]);

        Permission_role::create([
          'permission_id' => '5',
          'role_id' => '1',
        ]);

        Permission::create([
          'name' => 'update-roles',
          'display_name' => 'Редактирование ролей',
          'description' => 'Позволяет редактировать роли',
        ]);

        Permission_role::create([
          'permission_id' => '6',
          'role_id' => '1',
        ]);

        Permission::create([
          'name' => 'delete-roles',
          'display_name' => 'Удаление ролей',
          'description' => 'Позволяет удалять роли',
        ]);

        Permission_role::create([
          'permission_id' => '7',
          'role_id' => '1',
        ]);

        Permission::create([
          'name' => 'view-permissions',
          'display_name' => 'Просмотр списка привилегий',
          'description' => 'Позволяет просматривать список привилегий',
        ]);

        Permission_role::create([
          'permission_id' => '8',
          'role_id' => '1',
        ]);

        Permission::create([
          'name' => 'create-permissions',
          'display_name' => 'Создание привилегий',
          'description' => 'Позволяет создавать новые привилегии',
        ]);

        Permission_role::create([
          'permission_id' => '9',
          'role_id' => '1',
        ]);

        Permission::create([
          'name' => 'update-permissions',
          'display_name' => 'Редактирование привилегий',
          'description' => 'Позволяет редактировать привилегии',
        ]);

        Permission_role::create([
          'permission_id' => '10',
          'role_id' => '1',
        ]);

        Permission::create([
          'name' => 'delete-permissions',
          'display_name' => 'Удаление привилегий',
          'description' => 'Позволяет удалять привилегии',
        ]);

        Permission_role::create([
          'permission_id' => '11',
          'role_id' => '1',
        ]);

        Permission::create([
          'name' => 'view-system-settings',
          'display_name' => 'Просмотр системных настроек',
          'description' => 'Позволяет просматривать системные настройки',
        ]);

        Permission_role::create([
          'permission_id' => '12',
          'role_id' => '1',
        ]);
    }
}
