<?php

use Illuminate\Database\Seeder;

class CreateAdminUser extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          'name' => 'Crew Resource',
          'email' => 'admin@crew.ru',
            //Логин администратора, не забываем сменить после авторизации
          'password' => bcrypt('secret'),
            //Пароль администратора, не забываем сменить после авторизации
          'remember_token' => str_random(10),
        ]);
    }
}
