<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebmasterStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webmaster_statuses', function (Blueprint $table) {
            $table->integer('site_id')->unsigned();
            $table->string('host_id');
            $table->boolean('webmaster_verify_status')->default(false);
            $table->boolean('sitemap_status')->default(false);

            $table->foreign('site_id')->references('id')->on('sites')
              ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webmaster_statuses');
    }
}
