<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompsingsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compsings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('token_id');
            $table->string('name');
            $table->string('compsing_id')->unique();
            $table->boolean('compsing_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compsings');
    }
}
