<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbconnectsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dbconnects', function (Blueprint $table) {
            $table->integer('site_id')->unsigned();
            $table->string('dbname');
            $table->string('dbhost');
            $table->string('dbuser');
            $table->string('dbpass')->nullable();
            $table->string('dbport')->nullable();

            $table->foreign('site_id')->references('id')->on('sites')
              ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['site_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dbconnects');
    }
}
