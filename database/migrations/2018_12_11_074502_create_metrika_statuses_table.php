<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetrikaStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metrika_statuses', function (Blueprint $table) {
            $table->integer('site_id')->unsigned();
            $table->integer('counter_id')->unsigned();
            $table->boolean('metrika_verify_status')->default(false);

            $table->foreign('site_id')->references('id')->on('sites')
              ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metrika_statuses');
    }
}
