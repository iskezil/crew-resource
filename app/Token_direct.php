<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token_direct extends Model
{

    protected $fillable = [
      'user_id',
      'user_login',
      'token_name',
      'token',
      'token_status',
    ];
}
