<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Webmaster_status extends Model
{

    public $timestamps = false;

    public $primaryKey = 'site_id';
    protected $fillable = [
      'site_id',
      'host_id',
      'webmaster_verify_status',
      'sitemap_status',
    ];
}
