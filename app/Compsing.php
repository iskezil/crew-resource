<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compsing extends Model
{

    protected $fillable = [
      'user_id',
      'token_id',
      'name',
      'compsing_id',
      'compsing_status',
    ];
}
