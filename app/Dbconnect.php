<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Dbconnect extends Model
{

    public $timestamps = false;

    protected $fillable = [
      'site_id',
      'dbhost',
      'dbname',
      'dbuser',
      'dbpass',
      'dbport',
    ];

    public static function checkModule($code, $key)
    {
        try {
            $sitemapExtension = DB::connection('mysqlclient')
              ->select("SELECT * FROM oc_extension WHERE code='$code'");
            $sitemapExtension = json_decode(json_encode($sitemapExtension),
              true);
            if (isset($sitemapExtension[0]['code'])) {
                $sitemapSetting = DB::connection('mysqlclient')
                  ->select("SELECT * FROM oc_setting WHERE code='$code'");
                $sitemapSetting = json_decode(json_encode($sitemapSetting),
                  true);
            } else {
                $sitemapSetting = DB::connection('mysqlclient')
                  ->insert("INSERT INTO oc_extension VALUES (null, 'feed', '$code')");
            }

            if (isset($sitemapSetting[0]['value']) and $sitemapSetting[0]['value'] == 1) {
            } elseif (isset($sitemapSetting[0]['value'])) {
                DB::connection('mysqlclient')
                  ->insert("UPDATE oc_setting SET value = '1' WHERE code='$code'");
            } else {
                DB::connection('mysqlclient')
                  ->insert("INSERT INTO oc_setting VALUES (null, '0', '$code', '$key', '1', '0')");
            }
            return true;
        } catch (\Exception $errorInfo) {
            if ($errorInfo->getCode() == '42S22') {
                $errorMessage = 'Неверные поля в таблице';
            } elseif ($errorInfo->getCode() == '42S02') {
                $errorMessage = 'Название таблиц в базе данных не правильное';
            } elseif ($errorInfo->getCode() == '1044') {
                $errorMessage = 'Неверно указано имя базы данных или нет доступа для удалённого подключения';
            } elseif ($errorInfo->getCode() == '2002') {
                $errorMessage = 'Неверно указан Хост или порт базы данных или Сервер базы данных не доступен';
            } elseif ($errorInfo->getCode() == '1045') {
                $errorMessage = 'Неверно указан Логин от базы данных';
            } else {
                $errorMessage = 'Неизвестная ошибка при автоопределении sitemap, сообдщите код ошибки администратору сайта: ' . $errorInfo->getCode();
            }
            return $errorMessage;
        }


    }
}
