<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Permission;
use App\Permission_role;
use App\Role;
class AdminRoleController extends Controller
{
    public function index(){
        $data = Auth::user();
        if ($data->can('view-roles') === false) {
            abort(403);
        }
        $roles = Role::orderBy("id", 'desc')->get();
        return view('admin.system.roles.Role',compact(['roles', 'data']));
    }

    public function showformcreate()
    {
        $data = Auth::user();
        if ($data->can('create-roles') === false) {
            abort(403);
        }
        $permissions = Permission::all();
        return view('admin.system.roles.Formcreateroles',
          compact(['permissions', 'data']));
    }

    public function create(Request $request)
    {
        $data = Auth::user();
        if ($data->can('create-roles') === false) {
            abort(403);
        }

        $request->validate([
          'name' => 'required|string|unique:roles',
          'display_name' => 'required|string|unique:roles',
          'description' => 'required|string|unique:roles',
        ]);

        $role = Role::create($request->all());
        if ($request->permission) {
            foreach ($request->permission as $key => $value) {
                $role->attachPermission($value);
            }
        }
        return redirect()->route('roles.show')->with([
          'status' => 'Роль успешно добавлена!',
          'type' => 'success',
        ]);
    }

    public function showformupdate($id)
    {
        $data = Auth::user();
        if ($data->can('update-roles') === false) {
            abort(403);
        }
        $role = Role::find($id);
        $permissions = Permission::all();
        $role_permissions = $role->perms()->pluck('id', 'id')->toArray();

        return view('admin.system.roles.Formupdateroles',
          compact(['role', 'role_permissions', 'permissions', 'data']));
    }

    public function update(Request $request)
    {
        $data = Auth::user();
        if ($data->can('update-roles') === false) {
            abort(403);
        }

        $request->validate([
          'name' => 'required|string',
          'display_name' => 'required|string',
          'description' => 'required|string',
        ]);

        $role = Role::find($request->roleid);
        $role->update($request->all());

        Permission_role::where('role_id', $request->roleid)->delete();
        if ($request->permission) {
            foreach ($request->permission as $key => $value) {
                $role->attachPermission($value);
            }
        }
        return redirect()->route('roles.show')->with([
          'status' => 'Роль успешно обновлена!',
          'type' => 'success',
        ]);
    }

    public function delete(Request $request)
    {
        $data = Auth::user();
        if ($data->can('delete-roles') === false) {
            abort(403);
        }
        $id = $request->element_id;
        $role = Role::findOrFail($id);
        $role->delete();
    }
}
