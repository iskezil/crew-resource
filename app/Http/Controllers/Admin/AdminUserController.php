<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Role;
use App\Role_user;
use App\User;

class AdminUserController extends Controller
{

    public function index()
    {
        $data = Auth::user();
        if ($data->can('view-users') === false) {
            abort(403);
        }
        $users = User::orderBy("id", 'desc')->paginate(10);
        return view('admin.system.users.User', compact(['users', 'data']));
    }

    public function showformupdate($id)
    {
        $data = Auth::user();
        if ($data->can('update-users') === false) {
            abort(403);
        }
        $users = User::find($id);
        $roles = Role::all();
        $role_permissions = $users->roles()->pluck('id', 'id')->toArray();

        return view('admin.system.users.Formupdateusers',
          compact(['roles', 'role_permissions', 'users', 'data']));
    }

    public function update(Request $request)
    {
        $data = Auth::user();
        if ($data->can('update-users') === false) {
            abort(403);
        }
        $request->validate([
          'username' => 'required|string',
          'email' => 'required|email|string',
        ]);

        $id = $request->usersid;
        $UpdateUsers = User::find($id);
        $UpdateUsers->name = $request->username;
        $UpdateUsers->email = $request->email;
        $UpdateUsers->save();

        Role_user::where('user_id', $id)->delete();
        if ($request->role) {
            foreach ($request->role as $key => $value) {
                $UpdateUsers->attachRole($value);
            }
        }

        if (!empty($request->input('password'))) {
            $request->validate([
              'password' => 'required|confirmed|min:6',
            ]);

            $UpdateUsers = User::find($id);
            $UpdateUsers->password = Hash::make($request->password);
            $UpdateUsers->save();

        }
        return redirect()->route('users.show')->with([
          'status' => 'Ползователь успешно отредактирован!',
          'type' => 'success',
        ]);
    }

    public function delete(Request $request)
    {
        $data = Auth::user();
        if ($data->can('delete-users') === false) {
            abort(403);
        }
        $id = $request->element_id;
        $users = User::find($id)->delete();
    }
}
