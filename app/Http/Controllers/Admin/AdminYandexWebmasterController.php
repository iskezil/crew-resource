<?php

namespace App\Http\Controllers\Admin;

use App\Dbconnect;
use App\Ftpconnect;
use App\Site;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yandex\OAuth\Exception\AuthRequestException;
use App\Http\Traits\YandexTrait;
use Yandex\OAuth\OAuthClient;
use yandex\webmaster\api\webmasterApi;
use Storage;
use Config;
use App\Webmaster_status;

class AdminYandexWebmasterController extends Controller
{

    public function index()
    {
        $data = Auth::user();
        if ($data->YandexWebmasterToken == null) {
            YandexTrait::YandexAuthClient();
        }

        $sites = Site::all();
        return view('admin.yandex.webmaster.Webmaster',
          compact('sites', 'data'));
    }

    public function showformcreate()
    {
        $data = Auth::user();
        $ftpConnects = Ftpconnect::where('user_id', $data->id)->get();
        if ($ftpConnects->isEmpty()) {
            $ftpCheck = false;
        } else {
            $ftpCheck = true;
        }
        return view('admin.yandex.webmaster.Formcreatewebmaster',
          compact('ftpConnects', 'ftpCheck', 'data'));
    }

    public function create(Request $request)
    {

        /*
         * Получем данные авторизованного пользователя
         */
        $data = Auth::user();

        /*Производим валидацию пришедших данных из формы*/
        $request->validate([
          'name' => 'required|url',
          'webmaster' => 'required_unless:metrika,true',
          'metrika' => 'required_unless:webmaster,true',
          'ftphost' => 'required_if:type_ftp,new_ftp',
          'ftpuser' => 'required_if:type_ftp,new_ftp',
          'ftppass' => 'required_if:type_ftp,new_ftp',
          'ftpport' => 'required_if:type_ftp,new_ftp',
          'ftpdir' => 'required_if:type_ftp,new_ftp',
          'ftpname' => 'required_if:save_ftp,true',
          'dbhost' => 'required_if:sitemapauto,true',
          'dbname' => 'required_if:sitemapauto,true',
          'dbuser' => 'required_if:sitemapauto,true',
          'dbpass' => 'required_if:sitemapauto,true',
          'dbport' => 'required_if:sitemapauto,true',
          'sitemapurl' => 'required_if:sitemaplink,true',
        ]);

        if ($request->input('webmaster')) {

            /*Создаём массив с данными из формы*/
            $userdata = $request->input();

            /*Добавляем в массив id текущего пользователя*/
            $userdata['user_id'] = $data->id;

            /*Проверяем есть ли у него токен*/
            if ($data->YandexWebmasterToken == null) {
                YandexTrait::YandexAuthClient();
            }

            /*Записываем токен в переменную*/
            $accessToken = $data->YandexWebmasterToken;

            /*Иницализируем работу с Api*/
            $apiInit = webmasterApi::initApi($accessToken);//токен и user_id

            /*Добавляем новый хост в вебмастер*/
            $addHost = $apiInit->addHost($request->input('name'));//host_id

            /*Проверяем добавился ли сайт в вебмастер*/
            if (isset($addHost->error_code)) {
                if ($addHost->error_code == 'HOST_ALREADY_ADDED') {
                    $resultMessage['warning'][] = 'Cайт: ' . $request->input('name') . ' уже добавлен в вашем Y.Webmaster';
                } elseif ($addHost->error_code == 'HOSTS_LIMIT_EXCEEDED') {
                    $resultMessage['warning'][] = 'У Вас превышен лимит количества сайтов в списке сайтов Y.Webmaster (текущий лимит — 1703)';
                } else {
                    $resultMessage['error'][] = 'Приозошла неопознаная ошибка при добавлении сайта в Y.Webmaster';
                }
            } else {
                $resultMessage['success'][] = 'Сайт успешно добавлен в Y.Webmaster';

                /*Создаём запись в базе Sites который мы добавили в в Y.Webmaster*/
                $addSite = Site::create($userdata);

                /*Добавляем в массив id добавленного сайта из таблицы Sites*/
                $userdata['site_id'] = $addSite->id;

                /*Добавляем в массив host_id добавленного сайта в Y.Webmaster*/
                $userdata['host_id'] = $addHost->host_id;
            }


            /*Начинаем процесс верификации сайта Y.Webmaster если активен checkBox site_verify*/
            if ($request->input('site_verify')) {

                /*Парсим наш урл обрезаея https:// */
                $host = parse_url($request->input('name'));

                /*Проверяем тип работы с фтп для верификации*/
                if ($request->input('type_ftp') == 'new_ftp') {    /*Если нам пришли новые данные от фтп работаем с ними*/
                    /*Подменеям на лету конфиг работы с фтп*/
                    Config::set('filesystems.disks.ftp.host',
                      $request->input('ftphost'));
                    Config::set('filesystems.disks.ftp.username',
                      $request->input('ftpuser'));
                    Config::set('filesystems.disks.ftp.password',
                      $request->input('ftppass'));

                    /*Собираем путь до корня сайта для загрузки файла проверки*/
                    $fileDir = $request->input('ftpdir') . $host['host'];

                } elseif ($request->input('type_ftp') == 'exist_ftp') {    /*Если нам надо получить ранее сохраненные данные*/
                    $ftpInfo = Ftpconnect::find($request->input('select_exist_ftp'));   /*То получаем данные из таблицы ftpconnects*/

                    /*Подменеям на лету конфиг работы с фтп*/
                    Config::set('filesystems.disks.ftp.host',
                      $ftpInfo->ftphost);
                    Config::set('filesystems.disks.ftp.username',
                      $ftpInfo->ftpuser);
                    Config::set('filesystems.disks.ftp.password',
                      $ftpInfo->ftppass);

                    /*Собираем путь до корня сайта для загрузки файла проверки*/
                    $fileDir = $ftpInfo->ftpdir . $host['host'];
                }

                /*Получаем статус верификации + получаем верификационный код*/
                $hostVerify = $apiInit->checkVerification($addHost->host_id);

                /*Проверяем статус верификации*/
                if ($hostVerify->verification_state == 'VERIFIED') {
                    $resultMessage['warning'][] = 'Права на сайт: ' . $request->input('name') . ' уже подтверждены в Y.Webmaster';
                } elseif ($hostVerify->verification_state == 'IN_PROGRESS') {
                    $resultMessage['warning'][] = 'Процесс подтверждения сайта : ' . $request->input('name') . ' уже запущен в Y.Webmaster';
                } else {

                    /*Проверяем существует ли директория для сохранения проверочного файла*/
                    $fileDirExist = Storage::disk('ftp')->exists($fileDir);
                    if ($fileDirExist === false) {
                        $resultMessage['error'][] = 'Директория : ' . $fileDir . ' не найдена, верификация не возможна';
                    } else {
                        /*Описываем содержимое файла где $hostVerify->verification_uin и есть верификационый код*/
                        $content = '<html>
                                    <head>
                                    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
                                    </head>
                                    <body>Verification: ' . $hostVerify->verification_uin . '</body>
                                    </html>';

                        /*Записываем файл на фтп сервере*/
                        Storage::disk('ftp')
                          ->put($fileDir . '/yandex_' . $hostVerify->verification_uin . '.html',
                            $content);

                        /*Запускаем процесс валидации*/
                        @$apiInit->verifyHost($addHost->host_id, 'HTML_FILE');

                        /*Засыпаем на секунду так как процесс верификации занимает примерно столько времени*/
                        sleep(1);

                        /*Получаем статус верификации*/
                        $hostVerify = $apiInit->checkVerification($addHost->host_id);

                        /*Если статус равен как Верифицированный*/
                        if ($hostVerify->verification_state == 'VERIFIED') {
                            /*Добавляем статус верификации в массив с данными*/
                            $userdata['webmaster_verify_status'] = 1;
                            $resultMessage['success'][] = 'Верификация прошла успешно';
                        }

                        /*проверяем, сохранить ли данные от ftp*/
                        if ($request->input('save_ftp')) {
                            Ftpconnect::create($userdata);
                            $resultMessage['success'][] = 'Ftp данные успешно сохранены';
                        }
                    }

                }
                /*Проверяем добавлять ли sitemap*/
                if ($request->input('sitemap') and $request->input('sitemapauto')) {

                    /*Подменеям на лету конфиг работы с БД*/
                    Config::set('database.connections.mysqlclient.host',
                      $request->input('dbhost'));
                    Config::set('database.connections.mysqlclient.port',
                      $request->input('dbport'));
                    Config::set('database.connections.mysqlclient.database',
                      $request->input('dbname'));
                    Config::set('database.connections.mysqlclient.username',
                      $request->input('dbuser'));
                    Config::set('database.connections.mysqlclient.password',
                      $request->input('dbpass'));

                    /*Определяем тип сайта по содержимому его каталогов*/
                    if (Storage::disk('ftp')
                        ->exists($fileDir . '/admin/controller/feed/sitemap_pro.php') == true) {
                        /*Собираем Sitemap исходя из типа сайта*/
                        $sitemapUrl = $request->input('name') . '/index.php?route=feed/yandex_feed';
                        /*Проверяем активен ли Sitemap p.s см. Модель Dbconnect*/
                        $checkModule = Dbconnect::checkModule('sitemap_pro',
                          'sitemap_pro_status');

                    } elseif (Storage::disk('ftp')
                        ->exists($fileDir . '/admin/controller/feed/google_sitemap.php') == true) {
                        $sitemapUrl = $request->input('name') . '/index.php?route=feed/google_sitemap';
                        $checkModule = Dbconnect::checkModule('google_sitemap',
                          'google_sitemap_status');

                    } elseif (Storage::disk('ftp')
                        ->exists($fileDir . '/admin/controller/extension/feed/google_sitemap.php') == true) {
                        $sitemapUrl = $request->input('name') . '/index.php?route=extension/feed/google_sitemap';
                        $checkModule = Dbconnect::checkModule('google_sitemap',
                          'google_sitemap_status');

                    } elseif (Storage::disk('ftp')
                        ->exists($fileDir . '/engine/data/config.php') == true) {
                        $checkModule = true;
                        $sitemapUrl = $request->input('name') . '/sitemap.xml';
                    } else {
                        $checkModule = 'Не удалось добавить Sitemap в автоматическом режиме, укажите прямую ссылку';
                    }

                    /*Если модуль активен то добавляем Sitemap*/
                    if ($checkModule === true) {
                        $addSiteMap = $apiInit->addSitemap($addHost->host_id,
                          $sitemapUrl);
                    }

                    /*И сразу проверим добавился ли Sitemap*/
                    if (isset($addSiteMap->sitemap_id)) {
                        $resultMessage['success'][] = 'Sitemap успешно добавлен';
                        $userdata['sitemap_status'] = 1;
                    } else {
                        $resultMessage['error'][] = $checkModule;
                        $resultMessage['error'][] = 'Sitemap не добавлен';
                        $userdata['sitemap_status'] = 0;
                    }

                } elseif ($request->input('sitemap') and $request->input('sitemaplink')) {
                    $addSiteMap = $apiInit->addSitemap($addHost->host_id,
                      $request->input('sitemapurl'));
                    if (isset($addSiteMap->sitemap_id)) {
                        $resultMessage['success'][] = 'Sitemap успешно добавлен';
                        $userdata['sitemap_status'] = 1;
                    } else {
                        $resultMessage['error'][] = 'Sitemap не добавлен';
                        $userdata['sitemap_status'] = 0;
                    }
                }

                /*Ну и на последок проверяем есть ли в нашем массиве site_id и если есть то добавляем запись в базу*/
                if (isset($userdata['site_id'])) {
                    Webmaster_status::create($userdata);
                }

            }

        }
       
        return redirect()->route('webmasters.show')->with($resultMessage);
    }

    public function delete(Request $request)
    {
        $id = $request->element_id;
        $role = Site::findOrFail($id);
        $role->delete();
    }

    public function callback(Request $request)
    {
        $client = new OAuthClient(env('YANDEX_CLIENT_ID'),
          env('YANDEX_SECRET'));

        try {
            // осуществляем обмен
            $client->requestAccessToken($request->input('code'));
        } catch (AuthRequestException $ex) {
            echo $ex->getMessage();
        }

        // забираем полученный токен
        $accessToken['YandexWebmasterToken'] = $client->getAccessToken();
        /*
         * Сохраняем токен в базе
         */
        $user = Auth::user();
        $user->update($accessToken);
        return redirect()->route('webmasters.show')->with([
          'status' => 'Авторизация пройдена успешно!',
          'type' => 'success',
        ]);
    }
}
