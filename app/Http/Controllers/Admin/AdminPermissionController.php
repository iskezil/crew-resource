<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Permission;
use App\Permission_role;
use App\Role;

class AdminPermissionController extends Controller
{

    public function index()
    {
        $data = Auth::user();
        if ($data->can('view-permissions') === false) {
            abort(403);
        }
        $permissions = Permission::orderBy("id", 'desc')->get();
        return view('admin.system.permissions.Permission',
          compact(['permissions', 'data']));
    }

    public function showformcreate()
    {

        $data = Auth::user();
        if ($data->can('create-permissions') === false) {
            abort(403);
        }
        $roles = Role::all();
        return view('admin.system.permissions.Formcreatepermissions',
          compact(['roles', 'data']));
    }

    public function create(Request $request)
    {
        $data = Auth::user();
        if ($data->can('create-permissions') === false) {
            abort(403);
        }
        $request->validate([
          'name' => 'required|string|unique:permissions',
          'display_name' => 'required|string|unique:permissions',
          'description' => 'required|string|unique:permissions',
        ]);
        $permission = Permission::create($request->all());

        if ($request->role) {
            foreach ($request->role as $key => $value) {
                $role = Role::findOrFail($value);
                $role->attachPermission($permission);
            }
        }
        return redirect()->route('permissions.show')->with([
          'status' => 'Привилегия успешно добавлена!',
          'type' => 'success',
        ]);
    }

    public function showformupdate($id)
    {
        $data = Auth::user();
        if ($data->can('update-permissions') === false) {
            abort(403);
        }
        $permissions = Permission::find($id);
        $roles = Role::all();
        $role_permissions = $permissions->roles->pluck('id', 'id')->toArray();
        return view('admin.system.permissions.Formupdatepermissions',
          compact(['roles', 'role_permissions', 'permissions', 'data']));
    }

    public function update(Request $request)
    {
        $data = Auth::user();
        if ($data->can('update-permissions') === false) {
            abort(403);
        }

        $request->validate([
          'name' => 'required|string',
          'display_name' => 'required|string',
          'description' => 'required|string',
        ]);

        $permission = Permission::find($request->permissionid);
        $permission->update($request->all());

        Permission_role::where('permission_id', $request->permissionid)
          ->delete();
        if ($request->role) {
            foreach ($request->role as $key => $value) {
                $role = Role::findOrFail($value);
                $role->attachPermission($permission);
            }
        }

        return redirect()->route('permissions.show')->with([
          'status' => 'Привилегия успешно обновлена!',
          'type' => 'success',
        ]);
    }

    public function delete(Request $request)
    {
        $data = Auth::user();
        if ($data->can('delete-permissions') === false) {
            abort(403);
        }
        $id = $request->element_id;
        $role = Permission::findOrFail($id);
        $role->delete();
    }
}
