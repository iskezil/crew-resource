<?php

namespace App\Http\Controllers\Admin;

use App\Compsing;
use App\Token_direct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yandex\OAuth\Exception\AuthRequestException;
use Yandex\OAuth\OAuthClient;
use App\Http\Traits\YandexDirectTrait;
use Storage;
use Config;
class AdminYandexDirectController extends Controller
{

    use YandexDirectTrait;

    public function index()
    {
        $data = Auth::user();
        $userDirects = Token_direct::where('user_id', $data->id)
          ->where('token_status', 1)
          ->get();

        if (count($userDirects) == 0) {
            return redirect()->route('directs.create')->with([
              'status' => 'Для работы с личным кабинетом в директе нужно пройти авторизацию',
              'type' => 'success',
            ]);
        }

        foreach ($userDirects as $userDirect) {
            unset($this->apiErr);
            $this->accessToken = $userDirect->token;
            $this->userLogin = $userDirect->user_login;
            $Campaigns[$userDirect->user_login] = $this->getAllCampaigns();
            if (isset($this->apiErr)) {
                unset($Campaigns[$userDirect->user_login]);
                $campaignStatus[$userDirect->user_login] = 'danger';
                $i = 0;
                $Campaigns[$userDirect->user_login][$i] = (object)[
                  'Name' => $this->apiErr,
                  'StatusClarification' => $this->apiErr,
                ];
                $money[$userDirect->user_login] = false;
            } elseif ($Campaigns[$userDirect->user_login] == null) {
                $campaignStatus[$userDirect->user_login] = 'danger';
                $i = 0;
                $Campaigns[$userDirect->user_login][$i] = (object)[
                  'Name' => 'В аккаунте нет ни одной кампании!',
                  'StatusClarification' => 'В аккаунте нет ни одной кампании!',
                ];
                $money[$userDirect->user_login] = false;
            } else {
                $getMoney = $this->manage();
                $Campaigns[$userDirect->user_login] = $Campaigns[$userDirect->user_login]['Campaigns'];
                $money[$userDirect->user_login] = $getMoney['data']['Accounts'][0]['Amount'];
                foreach ($Campaigns[$userDirect->user_login] as $Campaign) {
                    if ($Campaign->StatusClarification == 'Идут показы') {
                        $campaignStatus[$userDirect->user_login] = 'success';
                    } else {
                        $campaignStatus[$userDirect->user_login] = 'danger';
                    }
                }
            }
        }
        return view('admin.yandex.direct.Direct',
          compact('userDirects', 'Campaigns', 'campaignStatus', 'data',
            'money'));
    }

    public function showformcreate()
    {
        $data = Auth::user();
        return view('admin.yandex.direct.Formcreatedirect', compact('data'));
    }

    public function create(Request $request)
    {
        $request->validate([
          'token_name' => 'required',
          'user_login' => 'required',
        ]);
        $client = new OAuthClient(env('YANDEX_DIRECT_CLIENT_ID'));
        //Передаём в запросе имя токена, чтобы Yandex в ответе его вернул
        $state = $request->input('token_name') . ":" . $request->input('user_login');
        $client->authRedirect(true, OAuthClient::CODE_AUTH_TYPE, $state);
    }

    public function showformrefresh(Request $request)
    {
        $data = Auth::user();
        if ($request->input('step_one')) {
            $userdata = $request->input();
            return view('admin.yandex.direct.Formrefreshdirect',
              compact('userdata', 'data'));
        } elseif ($request->input('step_two')) {
            $old_token_id = $request->input('old_token_id');
            $old_token_name = $request->input('old_token_name');
            $old_token = $request->input('old_token');
            $old_user_login = $request->input('old_user_login');
            $new_user_login = $request->input('new_user_login');
            $client = new OAuthClient(env('YANDEX_DIRECT_CLIENT_ID'));
            //Передаём в запросе имя токена, чтобы Yandex в ответе его вернул
            $state = $old_token_name . ":" . $new_user_login . ":" . $old_token_id . ":" . $old_token . ":" . $old_user_login;
            $client->authRedirect(true, OAuthClient::CODE_AUTH_TYPE, $state);
        }
    }

    public function checkshowformrefresh()
    {
        $data = Auth::user();
        return view('admin.yandex.direct.Formrefreshdirect', compact('data'));
    }

    public function refresh(Request $request)
    {
        /*Получаем инфу о юзере*/
        $data = Auth::user();
        $request->validate([
          'site_link' => 'required|url',
        ]);
        $new_link = $request->input('site_link');

        /*Получаем инфу о старом токене*/
        $old_token = Token_direct::find($request->input('old_token_id'));

        /*Получаем инфу о новом токене*/
        $new_token = Token_direct::find($request->input('new_token_id'));

        /*
         * Сразу загрузим шаблон сайта на наш сервер
         */

        /*Парсим название сайта*/
        $host = parse_url($new_link);

        /*Подменеям на лету конфиг работы с фтп*/
        Config::set('filesystems.disks.ftp.host',
          "192.168.0.100");
        Config::set('filesystems.disks.ftp.username',
          "gamparts");
        Config::set('filesystems.disks.ftp.password',
          "QWqw1989");

        /*Собираем путь до корня сайта для загрузки файла проверки*/
        $fileDir = "www/" . $host['host'];

        /*Прверяем создан ли сайт на сервере*/
        if (Storage::disk('ftp')->exists($fileDir) === false) {
            return redirect()->route('directs.show')->with([
              'status' => 'Сайт по пути: ' . $fileDir . " не найден.",
              'type' => 'danger',
            ]);
        }

        /*Проверяем существует ли шаблон*/
        if (Storage::disk('local')
            ->exists('templates/' . $new_token->token_name) === false) {
            return redirect()->route('directs.show')->with([
              'status' => 'Шаблон для кампании: ' . $new_token->token_name . " не найден.",
              'type' => 'danger',
            ]);
        }

        /*Все проверки пройдены, начинаем загружать шаблон в директорию сайта*/
        $directories = Storage::disk('local')
          ->allFiles('templates/' . $new_token->token_name);
        foreach ($directories as $directorie) {
            $len = strlen($new_token->token_name);
            $marker = stripos($directorie, $new_token->token_name);
            $filein = substr($directorie, $marker + $len);
            $fileout = $directorie;
            Storage::disk('ftp')
              ->put($fileDir . "/" . $filein,
                Storage::disk('local')->get($fileout));
            Storage::disk('ftp')->delete($fileDir . "/index.html");
        }

        /*Записываем токен старого рекламного кабинета*/
        $this->accessToken = $old_token->token;
        /*Записываем логин от старого рекламного кабинета*/
        $this->userLogin = $old_token->user_login;

        /*Получаем все сайтлинки*/
        $oldSitelinks = $this->getSitelinks();
        $oldreservSiteLink = json_decode(json_encode($oldSitelinks), true);

        /*Вырезаем Id они нам не понадобятся*/
        $i = 0;
        foreach ($oldSitelinks['SitelinksSets'] as $oldSitelink) {
            unset($oldSitelinks['SitelinksSets'][$i]->Id);


            $a = 0;
            foreach ($oldSitelinks['SitelinksSets'][$i]->Sitelinks as $oldlink) {
                $oldSitelinks['SitelinksSets'][$i]->Sitelinks[$a]->Href = $new_link;
                $a++;
            }
            $i++;
        }
        //        dump($oldSitelinks);
        //        die;
        /*Записываем токен нового рекламного кабинета*/
        $this->accessToken = $new_token->token;
        /*Записываем логин от нового рекламного кабинета*/
        $this->userLogin = $new_token->user_login;

        /*Формируем массив с новыми данными */
        $params = [
          'method' => 'add',
          'params' => $oldSitelinks,
        ];
        //                dump($params);

        /*Добвляем быстрые ссылки в новый аккаунт*/
        $newSitelinks = $this->addSitelinks($params);
        //                dump($newSitelinks);
        //                die;

        /*Записываем ид нового Сайтлинка в переменную*/
        $newSitelinksIds = $newSitelinks['AddResults'];
        //                    echo 'Новый Id сайтлинка';
        //                    dump($newSitelinksIds);

        /*Создаём массив oldToNewSitelinksId где ключ это старые Id а значение новые Id сайтлинков*/
        $i = 0;
        foreach ($newSitelinksIds as $newSitelinksId) {
            $oldToNewSitelinksId[$oldreservSiteLink['SitelinksSets'][$i]['Id']] = $newSitelinksId->Id;
            $i++;
        }
        //        dump($oldToNewSitelinksId);
        //        die;


        /*Записываем токен старого рекламного кабинета*/
        $this->accessToken = $old_token->token;
        /*Записываем логин от старого рекламного кабинета*/
        $this->userLogin = $old_token->user_login;

        /*Получаем уточнения*/
        $oldAdextensions = $this->getAdextensions();
        /*Проверяем есть ли вообще уточнения в старом кабинете, если есть то начинаем процесс добавления в новый кабинет*/
        if ($oldAdextensions != null) {
            /*Резервируем данные старой кампании*/
            $oldReservAdextensions = json_decode(json_encode($oldAdextensions),
              true);

            /*Вырезаем Id они нам не понадобятся*/
            $i = 0;
            foreach ($oldAdextensions['AdExtensions'] as $oldAdextension) {
                unset($oldAdextensions['AdExtensions'][$i]->Id);
                $i++;
            }

            /*Записываем токен нового рекламного кабинета*/
            $this->accessToken = $new_token->token;
            /*Записываем логин от нового рекламного кабинета*/
            $this->userLogin = $new_token->user_login;

            /*Формируем массив с новыми данными */
            $params = [
              'method' => 'add',
              'params' => $oldAdextensions,
            ];

            /*Добавляем уточнения в новый кабинет*/
            $newAdextensions = $this->addAdextensions($params);

            //        dump($newAdextensions);
            //die;

            /*Записываем ид новых уточнений в переменную*/
            $newAdextensionsIds = $newAdextensions['AddResults'];
            //                    echo 'Новый Id сайтлинка';
            //                    dump($newSitelinksIds);

            /*Создаём массив oldToNewAdextensionsIds где ключ это старые Id, а значение новые Id уточнений*/
            $i = 0;
            foreach ($newAdextensionsIds as $newAdextensionsId) {
                $oldToNewAdextensionsIds[$oldReservAdextensions['AdExtensions'][$i]['Id']] = $newAdextensionsId->Id;
                $i++;
            }
            //        dump($oldToNewAdextensionsIds);
            //        die;
        }


        /*Записываем токен старого рекламного кабинета*/
        $this->accessToken = $old_token->token;
        /*Записываем логин от старого рекламного кабинета*/
        $this->userLogin = $old_token->user_login;

        /*Получаем все изображения с визиток*/
        $oldAdImages = $this->getAdimages();
        //        dump($oldAdImages);

        // dump($oldAdImages);

        /*Формируем массив с параметрами для загрузки в новый кабинет*/
        $i = 0;
        $b = 0;
        
        /*Записываем токен нового рекламного кабинета*/
        $this->accessToken = $new_token->token;
        /*Записываем логин от нового рекламного кабинета*/
        $this->userLogin = $new_token->user_login;
        
        foreach ($oldAdImages['AdImages'] as $oldAdImage) {
            $oldImageData[$b]['ImageData'] = base64_encode(file_get_contents($oldAdImage->OriginalUrl));
            $oldImageData[$b]['Name'] = $oldAdImage->Name;
            $params = [
              'method' => 'add',
              'params' => [
                'AdImages' => $oldImageData,
              ],
            ];
        

        /*Сразу загрузим изображение Визиток так как они нам понадобятся*/
        $addImage = $this->addAdimages($params);
        $addImages['AddResults'][$i] =   $addImage['AddResults'][0];
            $i++;
        }
        // dump($addImages);

        /*Записываем токен старого рекламного кабинета*/
        $this->accessToken = $old_token->token;
        /*Записываем логин от старого рекламного кабинета*/
        $this->userLogin = $old_token->user_login;

        /*Получаем инвормацию о старых рекламных компаниях*/
        $compsings = $this->getTransferCampaigns();

        /*Записываем полученные данные в переменную*/
        $compsings = $compsings['Campaigns'];

        /*Начинаем обработку массива с данными старой рекламной компании*/
        foreach ($compsings as $compsing) {
            //            dump($compsing);

            /*Ансетим ненужные данные из массива*/
            unset($compsing->TimeTargeting->HolidaysSchedule);
            unset($compsing->TextCampaign->CounterIds);
            unset($compsing->TextCampaign->RelevantKeywords);

            $compsing->Notification->EmailSettings->Email = $new_token->user_login . "@yandex.ru";
            /*Создаём массив с настройками*/
            $settings = [
              "0" => [
                "Option" => "ADD_TO_FAVORITES",
                "Value" => "NO",
              ],
              "1" => [
                "Option" => "REQUIRE_SERVICING",
                "Value" => "NO",
              ],
              "2" => [
                "Option" => "MAINTAIN_NETWORK_CPC",
                "Value" => "YES",
              ],
              "3" => [
                "Option" => "ENABLE_SITE_MONITORING",
                "Value" => "NO",
              ],
              "4" => [
                "Option" => "ADD_METRICA_TAG",
                "Value" => "YES",
              ],
              "5" => [
                "Option" => "ADD_OPENSTAT_TAG",
                "Value" => "NO",
              ],
              "6" => [
                "Option" => "ENABLE_EXTENDED_AD_TITLE",
                "Value" => "YES",
              ],
              "7" => [
                "Option" => "ENABLE_COMPANY_INFO",
                "Value" => "NO",
              ],
              "8" => [
                "Option" => "EXCLUDE_PAUSED_COMPETING_ADS",
                "Value" => "NO",
              ],
              "9" => [
                "Option" => "ENABLE_AREA_OF_INTEREST_TARGETING",
                "Value" => "NO",
              ],
            ];

            /*Добавляем созданный массив в общий*/
            $compsing->TextCampaign->Settings = $settings;

            /*Создаём массив с данными новой рекламной комапании на основании полученных данных из старой компании*/
            $params = [
              'method' => 'add',
              'params' => [
                'Campaigns' => [
                  [
                    'ClientInfo' => $compsing->ClientInfo,
                    'Notification' => $compsing->Notification,
                    'TimeZone' => $compsing->TimeZone,
                    'Name' => $compsing->Name,
                    'StartDate' => date('Y-m-d'),
                    'DailyBudget' => $compsing->DailyBudget,
                    'NegativeKeywords' => $compsing->NegativeKeywords,
                    'TimeTargeting' => $compsing->TimeTargeting,
                    'TextCampaign' => $compsing->TextCampaign,

                  ],
                ],
              ],
            ];
            if ($params['params']['Campaigns'][0]['NegativeKeywords'] == null) {
                unset($params['params']['Campaigns'][0]['NegativeKeywords']);
            }
            /*Записываем токен нового рекламного кабинета*/
            $this->accessToken = $new_token->token;
            /*Записываем логин от нового рекламного кабинета*/
            $this->userLogin = $new_token->user_login;

            /*Создаём новую компанию на новом аккаунте*/
            $newCampaign = $this->addCampaigns($params);

            /*Проверяем на наличие ошибок*/
            if (isset($this->apiErr)) {
                return redirect()->route('directs.show')->with([
                  'status' => 'Обратите внимание камапании перенеслись не все из за ошибки: ' . $this->apiErr,
                  'type' => 'danger',
                ]);
            }
            /*Записываем новый Id компании в переменную*/
            $newCampaignId = $newCampaign['AddResults'][0]->Id;
            //            echo 'Id новой камапнии';
            //            dump($newCampaignId);

            /*Записываем токен старого рекламного кабинета*/
            $this->accessToken = $old_token->token;

            /*Записываем логин от старого рекламного кабинета*/
            $this->userLogin = $old_token->user_login;

            //            /*Получаем визитку с старого аккаунта*/
            //            $OldvCards = $this->getVcards();
            //
            //            /*Проверяем на наличие ошибок*/
            //            if (isset($this->apiErr)) {
            //                return redirect()->route('directs.show')->with([
            //                  'status' => 'Обратите внимание камапании перенеслись не все из за ошибки: ' . $this->apiErr,
            //                  'type' => 'danger',
            //                ]);
            //            }
            //            /*Записываем параметры старой визитки в переменную*/
            //            $OldvCards = $OldvCards['VCards'][1];
            //            //            echo 'Старая визитка';
            //            //            dump($OldvCards);
            //
            //            /*Собираем параметры новой визитки в кучу*/
            //            $OldvCards->CampaignId = $newCampaignId;
            //            $params = [
            //              'method' => 'add',
            //              'params' => [
            //                'VCards' => [
            //                  $OldvCards,
            //                ],
            //              ],
            //            ];
            //
            //            /*Записываем токен нового рекламного кабинета*/
            //            $this->accessToken = $new_token->token;
            //            /*Записываем логин от нового рекламного кабинета*/
            //            $this->userLogin = $new_token->user_login;
            //
            //            /*Создаём новую визитку в новом аккаунте*/
            //            $NewvCards = $this->addVcards($params);
            //
            //            /*Проверяем на наличие ошибок*/
            //            if (isset($this->apiErr)) {
            //                return redirect()->route('directs.show')->with([
            //                  'status' => 'Обратите внимание камапании перенеслись не все из за ошибки: ' . $this->apiErr,
            //                  'type' => 'danger',
            //                ]);
            //            }
            //            /*Записываем Id новой визитки в переменную*/
            //            $NewvCardsId = $NewvCards['AddResults'][0]->Id;
            //            //            echo 'Id новой визитки';
            //            //            dump($NewvCardsId);
            //            //die;

            /*Записываем токен старого рекламного кабинета*/
            $this->accessToken = $old_token->token;
            /*Записываем логин от старого рекламного кабинета*/
            $this->userLogin = $old_token->user_login;

            /*Получаем объявления из старого аккаунта*/
            $oldAds = $this->getAds($compsing->Id);
            //                        echo 'Массив со старыми объявлениями';
            //                        dump($oldAds);


            $i = 0;
            foreach ($oldAds['Ads'] as $ad) {
                $oldAds['Ads'][$i]->TextAd->Href = $new_link;
                $i++;
            }

            /*Проверяем на наличие ошибок*/
            if (isset($this->apiErr)) {
                return redirect()->route('directs.show')->with([
                  'status' => 'Обратите внимание камапании перенеслись не все из за ошибки: ' . $this->apiErr,
                  'type' => 'danger',
                ]);
            }
            /*Запрашиваем информаци сразу о всех группах объявлений отправляя массив с Id групп которые мы получили на 278 строке*/
            $oldAdGroups = $this->getAdGroups($compsing->Id);

            /*Проверяем на наличие ошибок*/
            if (isset($this->apiErr)) {
                return redirect()->route('directs.show')->with([
                  'status' => 'Обратите внимание камапании перенеслись не все из за ошибки: ' . $this->apiErr,
                  'type' => 'danger',
                ]);
            }
            /*Резервируем инфу о старых группах*/
            $oldReservAdGroups = json_decode(json_encode($oldAdGroups), true);
            //            echo 'Массив с информацией о старых группах';
            //            dump($oldAdGroups);

            /*Здесь мы в полученных группах подменяем Id старой кампании на Id новой кампании*/
            $i = 0;
            foreach ($oldAdGroups['AdGroups'] as $oldAdGroup) {
                unset($oldAdGroups['AdGroups'][$i]->Id);
                $oldAdGroups['AdGroups'][$i]->CampaignId = $newCampaignId;
                $i++;
            }
            //                        echo 'Массив с информацией о группах с новым Id кампании';
            //                        dump($oldAdGroups);
            //                        dump($oldReservAdGroups);
            //                        die;
            /*Формируем параметры для создания групп в новом аккаунте*/
            $params = [
              'method' => 'add',
              'params' => $oldAdGroups,
            ];

            /*Записываем токен нового рекламного кабинета*/
            $this->accessToken = $new_token->token;
            /*Записываем логин от нового рекламного кабинета*/
            $this->userLogin = $new_token->user_login;

            /*Создаём новые группы с привязкой к новой компании*/
            $NewAdGroup = $this->addAdGroups($params);

            /*Проверяем на наличие ошибок*/
            if (isset($this->apiErr)) {
                return redirect()->route('directs.show')->with([
                  'status' => 'Обратите внимание камапании перенеслись не все из за ошибки: ' . $this->apiErr,
                  'type' => 'danger',
                ]);
            }
            $AdGroupId = $NewAdGroup['AddResults'];
            //            echo 'Id новых групп';
            //            dump($AdGroupId);

            /*Создаём массив $oldToNewAdGroupId где имя ключа Id старой группы а значение равно Id новой группы*/
            $i = 0;
            foreach ($oldReservAdGroups['AdGroups'] as $oldReservAdGroupId) {
                $oldToNewAdGroupId[$oldReservAdGroupId['Id']] = $AdGroupId[$i]->Id;
                $i++;
            }
            //            echo 'имя ключа Id старой группы а значение равно Id новой группы';
            //            dump($oldToNewAdGroupId);


            /*Записываем токен старого рекламного кабинета*/
            $this->accessToken = $old_token->token;
            /*Записываем логин от старого рекламного кабинета*/
            $this->userLogin = $old_token->user_login;

            /*Получем кейворды из старой группы*/
            $oldKeyWords = $this->getkeywords($compsing->Id);

            /*Проверяем на наличие ошибок*/
            if (isset($this->apiErr)) {
                echo "502";
                die;
                return redirect()->route('directs.show')->with([
                  'status' => 'Обратите внимание камапании перенеслись не все из за ошибки: ' . $this->apiErr,
                  'type' => 'danger',
                ]);
            }
            /*Перебираем массив с кейфордами, и заменяем старый Id групп на Id новых групп путём подстановки в ключи массива Id старых групп*/
            $i = 0;
            foreach ($oldKeyWords['Keywords'] as $getKeyWord) {
                $oldKeyWords['Keywords'][$i]->AdGroupId = $oldToNewAdGroupId[$getKeyWord->AdGroupId];
                $i++;
            }
            //            echo 'Массив с старыми кейвордами но новыми Id групп';
            // dump($oldKeyWords);
            
            /*разбиваем кейворды на массивы по 900 ключей*/
            $oldKeyWords = array_chunk($oldKeyWords['Keywords'], 900);
            
             /*Записываем токен нового рекламного кабинета*/
            $this->accessToken = $new_token->token;
            /*Записываем логин от нового рекламного кабинета*/
            $this->userLogin = $new_token->user_login;
            
            /*Обрабатываем разбитый массив*/
            foreach($oldKeyWords as $oldKeyWord){
                /*Формируем массив с новыми данными кейвордов*/
                $params = [
                  'method' => 'add',
                  'params' => array(
                      'Keywords' => $oldKeyWord),
                ];
                
                /*Создаем наши кейворды с привязкой к новой группе*/
                $newKeyWords = $this->addkeywords($params);
                //            echo 'Новые кейворды';
                //            dump($newKeyWords);
                
                /*Подчищаем массив с отправленными данными*/
                unset($params);
                
                 /*Проверяем на наличие ошибок*/
                if (isset($this->apiErr)) {
                    return redirect()->route('directs.show')->with([
                      'status' => 'Обратите внимание камапании перенеслись не все из за ошибки: ' . $this->apiErr,
                      'type' => 'danger',
                    ]);
                }
            }
            

           
            

           
            $i = 0;
            foreach ($oldAds['Ads'] as $oldAd) {
                /*Проверяем есть ли в объявлении сайтлинки*/
                if ($oldAds['Ads'][$i]->TextAd->SitelinkSetId == null) {
                    /*Если нет то свойтво объекта*/
                    unset($oldAds['Ads'][$i]->TextAd->SitelinkSetId);
                } else {
                    /*Привязываем новый Id быстрых ссылок к объявлению*/
                    $oldAds['Ads'][$i]->TextAd->SitelinkSetId = $oldToNewSitelinksId[$oldAds['Ads'][$i]->TextAd->SitelinkSetId];
                }

                /*Проверяем есть ли в объявлении уточнения*/
                if ($oldAds['Ads'][$i]->TextAd->AdExtensions == null) {
                    /*Если нет то свойтво объекта*/
                    unset($oldAds['Ads'][$i]->TextAd->AdExtensions);
                } else {
                    /*Если есть то привязываем новые ид уточнений*/
                    $a = 0;
                    foreach ($oldAds['Ads'][$i]->TextAd->AdExtensions as $replaseAdExtensions) {
                        unset($oldAds['Ads'][$i]->TextAd->AdExtensions[$a]->Type);
                        $oldAds['Ads'][$i]->TextAd->AdExtensionIds[$a] = $oldToNewAdextensionsIds[$oldAds['Ads'][$i]->TextAd->AdExtensions[$a]->AdExtensionId];
                        $a++;
                    }
                    /*И удаляем ненужное нам свойство объекта*/
                    unset($oldAds['Ads'][$i]->TextAd->AdExtensions);

                }

                /*Привязываем визитку ко всем объявленияем*/
                //                $oldAds['Ads'][$i]->TextAd->VCardId = $NewvCardsId;
                unset($oldAds['Ads'][$i]->TextAd->VCardId);

                /*Привязываем объявление к нужной группе*/
                $oldAds['Ads'][$i]->AdGroupId = $oldToNewAdGroupId[$oldAds['Ads'][$i]->AdGroupId];
                $i++;
            }
            //            echo 'Новые параметры';
            //            dump($oldAds);

            /*Формируем параметры для создания объявления*/
            $params = [
              'method' => 'add',
              'params' => $oldAds,
            ];
            
            /*Создаём новые объявление*/
            $newAds = $this->addAds($params);
            //            echo 'Новое объявление';
            //          dump($newAds);
            //            die;
            /*Проверяем на наличие ошибок*/
            if (isset($this->apiErr)) {
                return redirect()->route('directs.show')->with([
                  'status' => 'Обратите внимание камапании перенеслись не все из за ошибки: ' . $this->apiErr,
                  'type' => 'danger',
                ]);
            }
            /*Забираем Id новых объявлений и складываем в массив*/
            foreach ($newAds['AddResults'] as $newAd) {
                $newAdId[] = $newAd->Id;
            }

            /*Формуриуем параметры для отправки на модерацию*/
            $params = [
              'method' => 'moderate',
              'params' => [
                'SelectionCriteria' => [
                  'Ids' => $newAdId,
                ],
              ],
            ];

            /*Отправляем созданные объявления на модерацию*/
            $moderate = $this->moderate($params);
            //            dump($moderate);


            /*Проверяем на наличие ошибок*/
            if (isset($this->apiErr)) {
                return redirect()->route('directs.show')->with([
                  'status' => 'Обратите внимание камапании перенеслись не все из за ошибки: ' . $this->apiErr,
                  'type' => 'danger',
                ]);
            }
            /*Записываем токен старого рекламного кабинета*/
            $this->accessToken = $old_token->token;
            /*Записываем логин от старого рекламного кабинета*/
            $this->userLogin = $old_token->user_login;

            /*Останваливаем перенесенную кампанию*/
            $params = [
              'method' => 'suspend',
              'params' => [
                'SelectionCriteria' => [
                  'Ids' => [
                    $compsing->Id,
                  ],
                ],
              ],
            ];
            $suspendOldCampaigns = $this->suspendCampaigns($params);

            /*Проверяем на наличие ошибок*/
            if (isset($this->apiErr)) {
                return redirect()->route('directs.show')->with([
                  'status' => 'Обратите внимание камапании перенеслись не все из за ошибки: ' . $this->apiErr,
                  'type' => 'danger',
                ]);
            }
            if (isset($this->apiErr)) {
                return redirect()->route('directs.show')->with([
                  'status' => 'Обратите внимание камапании перенеслись не все из за ошибки: ' . $this->apiErr,
                  'type' => 'danger',
                ]);
                die;
            }
            /*Добавляем струю каманию в архив*/
            $archiveOld = Compsing::where('compsing_id', $compsing->Id)
              ->first();
            $archiveOld->compsing_status = 0;
            $archiveOld->save();

            $compsingData['user_id'] = $data->id;
            $compsingData['token_id'] = $new_token->id;
            $compsingData['name'] = $archiveOld->name;
            $compsingData['compsing_id'] = $newCampaignId;
            $compsingData['compsing_status'] = 1;
            Compsing::create($compsingData);

            /*Подчищаем данные для следующей итерации*/
            unset($oldGroupIds);
            unset($oldSitelinkSetId);
            unset($newAdId);
        }


        /*После переноса отправляем старую кампанию в архив*/
        $UpdateOldToken = Token_direct::find($old_token->id);
        $UpdateOldToken->token_status = 0;
        $UpdateOldToken->save();

        /*Выводим юзеру сообщение что всё окей*/
        return redirect()->route('directs.show')->with([
          'status' => 'Аккаунт успешно перенесен',
          'type' => 'success',
        ]);
    }

    public function refreshToken(Request $request)
    {
        $token_id = $request->input('token_id');
        $client = new OAuthClient(env('YANDEX_DIRECT_CLIENT_ID'));
        $state = $token_id . ":update";
        $client->authRedirect(true, OAuthClient::CODE_AUTH_TYPE, $state);
    }

    public function callback(Request $request)
    {
        $data = Auth::user();
        $client = new OAuthClient(env('YANDEX_DIRECT_CLIENT_ID'),
          env('YANDEX_DIRECT_SECRET'));

        try {
            // осуществляем обмен
            $client->requestAccessToken($request->input('code'));
        } catch (AuthRequestException $ex) {
            echo $ex->getMessage();
        }
        //Разбиваем строку state в массив
        $userState = explode(":", $request->input('state'));
        if (isset($userState[4])) {
            //Собираем массив с параметрами
            $userdata['user_id'] = $data->id;
            $userdata['token_name'] = $userState[0];
            $userdata['user_login'] = $userState[1];
            $userdata['token'] = $client->getAccessToken();
            $userdata['token_status'] = 1;

            if (Token_direct::where('token', $client->getAccessToken())
                ->orWhere('user_login', $userdata['user_login'])
                ->exists() == true) {
                return redirect()->route('directs.show')->with([
                  'status' => 'Поздравляю! Вы долбень, данный аккаунт уже добавлен',
                  'type' => 'danger',
                ]);
            } else {
                $new_token = Token_direct::create($userdata);
                return redirect()->route('directs.check.refresh')->with([
                  'status' => 'Аккаунт успешно добавлен',
                  'type' => 'success',
                  'new_token_id' => $new_token->id,
                  'new_user_login' => $userState[1],
                  'old_token_id' => $userState[2],
                  'old_user_name' => $userState[4],
                ]);
            }
        } elseif (isset($userState[1]) && $userState[1] == 'update') {
            $newToken = Token_direct::find($userState[0]);
            $newToken->token = $client->getAccessToken();
            $newToken->save();
            return redirect()->route('directs.show')->with([
              'status' => 'Токен успешно обновлён',
              'type' => 'success',
            ]);
        } elseif (isset($userState[1]) && $userState[1] != 'update') {
            //Собираем массив с параметрами
            $userdata['user_id'] = $data->id;
            $userdata['token_name'] = $userState[0];
            $userdata['user_login'] = $userState[1];
            $userdata['token'] = $client->getAccessToken();
            $userdata['token_status'] = 1;

            /*
             * Сохраняем токен в базе если он ещё не сохранён
             */
            if (Token_direct::where('token', $client->getAccessToken())
                ->orWhere('user_login', $userdata['user_login'])
                ->exists() == true) {
                return redirect()->route('directs.show')->with([
                  'status' => 'Поздравляю! Вы долбень, данный аккаунт уже добавлен',
                  'type' => 'danger',
                ]);
            } else {
                $newToken = Token_direct::create($userdata);

                /*Записываем токен старого рекламного кабинета*/
                $this->accessToken = $userdata['token'];
                /*Записываем логин от старого рекламного кабинета*/
                $this->userLogin = $userdata['user_login'];

                /*Получаем инвормацию о рекламных компаниях*/
                $compsings = $this->getAllCampaigns();

                /*Записываем информацию о рекламных кампаний*/
                foreach ($compsings['Campaigns'] as $compsing) {
                    $compsingName = explode(";", $compsing->Name);
                    $compsingData['user_id'] = $data->id;
                    $compsingData['token_id'] = $newToken->id;
                    $compsingData['name'] = $compsingName[0];
                    $compsingData['compsing_id'] = $compsing->Id;
                    $compsingData['compsing_status'] = 1;
                    Compsing::create($compsingData);
                }

                return redirect()->route('directs.show')->with([
                  'status' => 'Аккаунт успешно добавлен',
                  'type' => 'success',
                ]);
            }
        } else {
            return redirect()->route('directs.show')->with([
              'status' => 'Что то пошло не так...',
              'type' => 'danger',
            ]);
        }


    }

    public function archive(Request $request)
    {
        $data = Auth::user();
        $id = $request->element_id;
        $UpdateOldToken = Token_direct::find($id);
        $UpdateOldToken->token_status = 0;
        $UpdateOldToken->save();
    }

    public function statistics()
    {
        $data = Auth::user();
        return view('admin.yandex.direct.FormGetStatistics', compact('data'));
    }

    public function getStatistic(Request $request)
    {
        $start_date = explode('/', $request->input('start'));
        $start_date = $start_date[2] . '-' . $start_date[0] . '-' . $start_date[1];
        $end_date = explode('/', $request->input('end'));
        $end_date = $end_date[2] . '-' . $end_date[0] . '-' . $end_date[1];

        $allSumm = 0;
        $companySum = false;
        $errorLog = false;
        $data = Auth::user();
        $userDirects = Token_direct::where('user_id', $data->id)
          ->where('created_at', '>', $start_date)
          ->where('created_at', '<', $end_date)
          ->get();

        foreach ($userDirects as $userDirect) {
            $this->accessToken = $userDirect->token;
            $this->userLogin = $userDirect->user_login;
            $cost = $this->getStatistics($start_date, $end_date);

            if (isset($this->apiErr)) {
                $errorLog .= "Произошла ошибка: " . $this->apiErr . " при получении данных с аккаунта: " . $userDirect->user_login . "<br>";
            }
            $companySum .= "Аккаунт " . $userDirect->user_login . " потратил: " . $cost . " Рублей. <br>";
            $allSumm += $cost;
            sleep(1);
        }
        if ($errorLog === false) {
            return redirect()->route('directs.statistics')->with([
              'status' => 'За выбраный период всего было потрачено: ' . $allSumm . " Рублей. <br> детальный отчёт: <br>" . $companySum,
              'type' => 'success',
            ]);
        } else {
            return redirect()->route('directs.statistics')->with([
              'status' => 'За выбраный период всего было потрачено: ' . $allSumm . " Рублей. <br> детальный отчёт: <br>" . $companySum,
              'error' => $errorLog,
              'type' => 'success',
            ]);
        }

    }
}
