<?php

namespace App\Http\Traits;

trait YandexDirectTrait
{

    private $accessToken;

    public $userLogin;

    public $apiErr;

    public $apiUrl = 'https://api.direct.yandex.com/json/v5';


    public function post($resource, $params)
    {
        //--- Подготовка и выполнение запроса -----------------------------------//
        /*Получение url обращения*/
        $url = $this->getApiUrl($resource);
        // Установка HTTP-заголовков запроса
        $headers = [
          "Authorization: Bearer $this->accessToken",
            // OAuth-токен. Использование слова Bearer обязательно
          "Client-Login: $this->userLogin",
            // Логин клиента рекламного агентства
          "Accept-Language: ru",
            // Язык ответных сообщений
          "Content-Type: application/json; charset=utf-8"
            // Тип данных и кодировка запроса
        ];

        // Преобразование входных параметров запроса в формат JSON
        $body = json_encode($params,
          JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);


        // Инициализация cURL
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $body);

        /*
        Для полноценного использования протокола HTTPS можно включить проверку SSL-сертификата сервера API Директа.
        Чтобы включить проверку, установите опцию CURLOPT_SSL_VERIFYPEER в true, а также раскомментируйте строку с опцией CURLOPT_CAINFO и укажите путь к локальной копии корневого SSL-сертификата.
        */
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($curl, CURLOPT_CAINFO, getcwd().'\CA.pem');

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        // Выполнение запроса, получение результата
        $result = curl_exec($curl);

        //--- Обработка результата выполнения запроса ---------------------------//
        if (!$result) {
            echo('Ошибка cURL: ' . curl_errno($curl) . ' - ' . curl_error($curl));
        } else {
            // Разделение HTTP-заголовков и тела ответа
            $responseHeadersSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            $responseHeaders = substr($result, 0, $responseHeadersSize);
            $responseBody = substr($result, $responseHeadersSize);

            if (curl_getinfo($curl, CURLINFO_HTTP_CODE) != 200) {
                echo "HTTP-ошибка: " . curl_getinfo($curl, CURLINFO_HTTP_CODE);
            } else {
                // Преобразование ответа из формата JSON
                $responseBody = json_decode($responseBody);

                if (isset($responseBody->error)) {
                    // Извлечение HTTP-заголовков ответа: RequestId (Id запроса) и Units (информация о баллах)
                    //                    $responseHeadersArr = explode("\r\n", $responseHeaders);
                    //                    //                    dump($responseHeadersArr);
                    //                    //                    foreach ($responseHeadersArr as $header) {
                    //                    //                        if (preg_match('/(Units|Units-Used-Login):/',
                    //                    //                          $header)) {
                    //                    //                            echo "$header <br>";
                    //                    //                        }
                    //                    //                    }
                    $apiErr = $responseBody->error;
                    $this->apiErr = "Ошибка API {$apiErr->error_code}: {$apiErr->error_string} - {$apiErr->error_detail} (RequestId: {$apiErr->request_id})";

                } else {
                    // Извлечение HTTP-заголовков ответа: RequestId (Id запроса) и Units (информация о баллах)
                    $responseHeadersArr = explode("\r\n", $responseHeaders);
                    //                    dump($responseHeadersArr);
                    //                    foreach ($responseHeadersArr as $header) {
                    //                        if (preg_match('/(Units|Units-Used-Login):/',
                    //                          $header)) {
                    //                            echo "$header <br>";
                    //                        }
                    //                    }
                    // Отдаём результат
                    return (array)$responseBody->result;
                }
            }
        }

        //--- Отладочная информация ---------------------------------------------//
        //echo "<hr>Заголовки запроса: <pre>".curl_getinfo($curl, CURLINFO_HEADER_OUT)."</pre>";
        //echo "Запрос: <pre>".json_encode($params, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)."</pre>";
        //echo "Заголовки ответа: <pre>".$responseHeaders."</pre>";
        //echo "Ответ: <pre>".json_encode($responseBody, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)."</pre>";

        curl_close($curl);
    }

    public function manage()
    {
        $ch = curl_init();
        $params = [
          'SelectionCriteria' => (object)[],
          'Action' => 'Get',
        ];
        $request = [
          'token' => $this->accessToken,
          'method' => 'AccountManagement',
          'param' => $params,
          'locale' => 'ru',
        ];
        $request = json_encode($request);

        curl_setopt($ch, CURLOPT_URL,
          'https://api.direct.yandex.ru/live/v4/json/');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
          "POST /json/v5/ads/ HTTP/1.1",
          "Host: api.direct.yandex.com",
          "Authorization: Bearer $this->accessToken",
          "Accept-Language: ru",
          "Client-Login:  $this->userLogin@yandex.ru",
          "Content-Type: application/json; charset=utf-8",
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $server_output = curl_exec($ch);
        curl_close($ch);
        $balance_direct_miramall = json_decode($server_output, true);
        return $balance_direct_miramall;
    }

    public function getApiUrl($resource)
    {
        $apiUrl = $this->apiUrl;
        return $apiUrl . $resource;
    }

    public function getAllCampaigns()
    {
        $params = [
          'method' => 'get',
            // Используемый метод сервиса Campaigns
          'params' => [
            'SelectionCriteria' => [
              "States" => ['ON', 'OFF', 'ENDED', 'SUSPENDED'],
            ],
              // Критерий отбора кампаний. Для получения всех кампаний должен быть пустым
            'FieldNames' => [
              "BlockedIps",
              "ExcludedSites",
              "Currency",
              "DailyBudget",
              "Notification",
              "EndDate",
              "Funds",
              "ClientInfo",
              "Id",
              "Name",
              "NegativeKeywords",
              "RepresentedBy",
              "StartDate",
              "Statistics",
              "State",
              "Status",
              "StatusPayment",
              "StatusClarification",
              "SourceId",
              "TimeTargeting",
              "TimeZone",
              "Type",
            ],
              // Названия параметров, которые требуется получить
            "TextCampaignFieldNames" => [
              "CounterIds",
              "RelevantKeywords",
              "Settings",
              "BiddingStrategy",
            ],
          ],
        ];
        return $this->post('/campaigns', $params);
    }

    public function getTransferCampaigns()
    {
        $params = [
          'method' => 'get',
            // Используемый метод сервиса Campaigns
          'params' => [
            'SelectionCriteria' => [
              "States" => ['ON', 'ENDED'],
            ],
              // Критерий отбора кампаний. Для получения всех кампаний должен быть пустым
            'FieldNames' => [
              "BlockedIps",
              "ExcludedSites",
              "Currency",
              "DailyBudget",
              "Notification",
              "EndDate",
              "Funds",
              "ClientInfo",
              "Id",
              "Name",
              "NegativeKeywords",
              "RepresentedBy",
              "StartDate",
              "Statistics",
              "State",
              "Status",
              "StatusPayment",
              "StatusClarification",
              "SourceId",
              "TimeTargeting",
              "TimeZone",
              "Type",
            ],
              // Названия параметров, которые требуется получить
            "TextCampaignFieldNames" => [
              "CounterIds",
              "RelevantKeywords",
              "Settings",
              "BiddingStrategy",
            ],
          ],
        ];
        return $this->post('/campaigns', $params);
    }


    public function addCampaigns($params)
    {
        return $this->post('/campaigns', $params);
    }

    public function archiveCampaigns($params)
    {
        return $this->post('/campaigns', $params);
    }

    public function suspendCampaigns($params)
    {
        return $this->post('/campaigns', $params);
    }

    public function getVcards()
    {
        $params = [
          'method' => 'get',
            // Используемый метод сервиса Campaigns
          'params' => [    // Критерий отбора кампаний. Для получения всех кампаний должен быть пустым
            'FieldNames' => [
              "Country",
              "City",
              "Street",
              "House",
              "Building",
              "Apartment",
              "CompanyName",
              "ExtraMessage",
              "ContactEmail",
              "CampaignId",
              "Ogrn",
              "WorkTime",
              "InstantMessenger",
              "Phone",
              "PointOnMap",
            ],          // Названия параметров, которые требуется получить
          ],
        ];
        return $this->post('/vcards', $params);
    }

    public function addVcards($params)
    {
        return $this->post('/vcards', $params);
    }

    public function getAds($CampaignIds)
    {
        $params = [
          'method' => 'get',
            // Используемый метод сервиса Campaigns
          'params' => [    // Критерий отбора кампаний. Для получения всех кампаний должен быть пустым
            'SelectionCriteria' => [
              'CampaignIds' => [$CampaignIds],
            ],
            'FieldNames' => ["AdGroupId"],
              // Названия параметров, которые требуется получить
            'TextAdFieldNames' => [
              "Title",
              "Title2",
              "Text",
              "Href",
              "Mobile",
              "DisplayUrlPath",
              "VCardId",
              "SitelinkSetId",
              "AdImageHash",
              "VideoExtension",
              "AdExtensions",
            ],
              // Названия параметров, которые требуется получить
            'MobileAppAdFieldNames' => [
              "Title",
              "Text",
              "Features",
              "Action",
              "AdImageHash",
              "AdImageModeration",
              "TrackingUrl",
            ],
              // Названия параметров, которые требуется получить
            'DynamicTextAdFieldNames' => [
              "Text",
              "VCardId",
              "VCardModeration",
              "SitelinkSetId",
              "SitelinksModeration",
              "AdImageHash",
              "AdImageModeration",
              "AdExtensions",
            ],
              // Названия параметров, которые требуется получить
            'TextImageAdFieldNames' => ["AdImageHash", "Href"],
              // Названия параметров, которые требуется получить
            'MobileAppImageAdFieldNames' => ["AdImageHash", "TrackingUrl"],
              // Названия параметров, которые требуется получить
            'TextAdBuilderAdFieldNames' => ["Creative", "Href"],
              // Названия параметров, которые требуется получить
            'MobileAppAdBuilderAdFieldNames' => ["Creative", "TrackingUrl"],
              // Названия параметров, которые требуется получить
            'CpcVideoAdBuilderAdFieldNames' => ["Creative", "Href"],
              // Названия параметров, которые требуется получить
            'CpmBannerAdBuilderAdFieldNames' => [
              "Creative",
              "Href",
              "TrackingPixels",
            ],
              // Названия параметров, которые требуется получить
              // Названия параметров, которые требуется получить

          ],
        ];
        return $this->post('/ads', $params);
    }

    public function addAds($params)
    {
        return $this->post('/ads', $params);
    }

    public function getSitelinks()
    {
        $params = [
          'method' => 'get',
            // Используемый метод сервиса Campaigns
          'params' => [    // Критерий отбора кампаний. Для получения всех кампаний должен быть пустым
            'FieldNames' => ["Sitelinks", "Id"],
              // Названия параметров, которые требуется получить


          ],
        ];
        return $this->post('/sitelinks', $params);
    }

    public function addSitelinks($params)
    {
        return $this->post('/sitelinks', $params);
    }

    public function getAdGroups($CampaignIds)
    {
        $params = [
          'method' => 'get',
            // Используемый метод сервиса Campaigns
          'params' => [    // Критерий отбора кампаний. Для получения всех кампаний должен быть пустым
            'SelectionCriteria' => [
              'CampaignIds' => [$CampaignIds],
            ],
            'FieldNames' => [
              "CampaignId",
              "Id",
              "Name",
              "NegativeKeywords",
              "RegionIds",
              "TrackingParams",
            ],          // Названия параметров, которые требуется получить
            'MobileAppAdGroupFieldNames' => [
              "StoreUrl",
              "TargetDeviceType",
              "TargetCarrier",
              "TargetOperatingSystemVersion",
              "AppIconModeration",
              "AppAvailabilityStatus",
              "AppOperatingSystemType",
            ],          // Названия параметров, которые требуется получить
            'DynamicTextAdGroupFieldNames' => [
              "DomainUrl",
              "DomainUrlProcessingStatus",
            ],          // Названия параметров, которые требуется получить
            'DynamicTextFeedAdGroupFieldNames' => [
              "Source",
              "SourceType",
              "SourceProcessingStatus",
            ],          // Названия параметров, которые требуется получить
          ],
        ];
        return $this->post('/adgroups', $params);
    }

    public function addAdGroups($params)
    {
        return $this->post('/adgroups', $params);
    }

    public function getkeywords($compsingId)
    {
        $params = [
          'method' => 'get',
            // Используемый метод сервиса Campaigns
          'params' => [    // Критерий отбора кампаний. Для получения всех кампаний должен быть пустым
            'SelectionCriteria' => [
              'CampaignIds' => [$compsingId],
            ],
            'FieldNames' => [
              "Keyword",
              "Bid",
              "ContextBid",
              "StrategyPriority",
              "UserParam1",
              "UserParam2",
              "AdGroupId",
            ],
          ],
        ];
        return $this->post('/keywords', $params);
    }

    public function addkeywords($params)
    {

        return $this->post('/keywords', $params);
    }

    public function getAdimages()
    {
        $params = [
          'method' => 'get',
            // Используемый метод сервиса Campaigns
          'params' => [    // Критерий отбора кампаний. Для получения всех кампаний должен быть пустым
            'FieldNames' => [
              "OriginalUrl",
              "Name",
            ],
          ],
        ];
        return $this->post('/adimages', $params);
    }

    public function addAdimages($params)
    {
        return $this->post('/adimages', $params);
    }

    public function moderate($params)
    {
        return $this->post('/ads', $params);
    }

    public function getAdextensions()
    {
        $params = [
          'method' => 'get',
            // Используемый метод сервиса Campaigns
          'params' => [    // Критерий отбора кампаний. Для получения всех кампаний должен быть пустым
            'SelectionCriteria' => [
              'States' => ['ON'],
            ],
            'FieldNames' => [
              "Id",
            ],
            'CalloutFieldNames' => [
              "CalloutText",
            ],
          ],
        ];
        return $this->post('/adextensions', $params);
    }

    public function addAdextensions($params)
    {
        return $this->post('/adextensions', $params);
    }

    public function getStatistics($start_date, $end_date)
    {
        // Настройки для вывода содержимого буфера, которые позволяют делать вывод на экран
        // при использовании функции sleep
        ob_implicit_flush();

        //--- Входные данные ---------------------------------------------------//
        // Адрес сервиса Reports для отправки JSON-запросов (регистрозависимый)
        $url = 'https://api.direct.yandex.com/json/v5/reports';


        //--- Подготовка запроса -----------------------------------------------//
        // Создание тела запроса
        $params = [
          "params" => [
            "SelectionCriteria" => [
              "DateFrom" => $start_date,
              "DateTo" => $end_date,
            ],
            "FieldNames" => ["Date", "CampaignId", "Clicks", "Cost"],
            "ReportName" => "НАЗВАНИЕ_ОТЧЕТА",
            "ReportType" => "CAMPAIGN_PERFORMANCE_REPORT",
            "DateRangeType" => "CUSTOM_DATE",
            "Format" => "TSV",
            "IncludeVAT" => "YES",
            "IncludeDiscount" => "YES",
          ],
        ];

        // Преобразование входных параметров запроса в формат JSON
        $body = json_encode($params);

        // Создание HTTP-заголовков запроса
        $headers = [
            // OAuth-токен. Использование слова Bearer обязательно
          "Authorization: Bearer $this->accessToken",
            // Логин клиента рекламного агентства
          "Client-Login: $this->userLogin",
            // Язык ответных сообщений
          "Accept-Language: ru",
            // Режим формирования отчета
          "processingMode: auto",
            // Формат денежных значений в отчете
          "returnMoneyInMicros: false",
            // Не выводить в отчете строку с названием отчета и диапазоном дат
            // "skipReportHeader: true",
            // Не выводить в отчете строку с названиями полей
            // "skipColumnHeader: true",
            // Не выводить в отчете строку с количеством строк статистики
            // "skipReportSummary: true"
        ];

        // Инициализация cURL
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $body);

        /*
        Для полноценного использования протокола HTTPS можно включить проверку SSL-сертификата сервера API Директа.
        Чтобы включить проверку, установите опцию CURLOPT_SSL_VERIFYPEER в true, а также раскомментируйте строку с опцией CURLOPT_CAINFO и укажите путь к локальной копии корневого SSL-сертификата.
        */
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_CAINFO, getcwd().'\CA.pem');

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        // --- Запуск цикла для выполнения запросов ---
        // Если получен HTTP-код 200, то выводится содержание отчета
        // Если получен HTTP-код 201 или 202, выполняются повторные запросы
        while (true) {
            $result = curl_exec($curl);

            if (!$result) {

                echo('Ошибка cURL: ' . curl_errno($curl) . ' - ' . curl_error($curl));

                break;

            } else {

                // Разделение HTTP-заголовков и тела ответа
                $responseHeadersSize = curl_getinfo($curl,
                  CURLINFO_HEADER_SIZE);
                $responseHeaders = substr($result, 0, $responseHeadersSize);
                $responseBody = substr($result, $responseHeadersSize);

                // Получение кода состояния HTTP
                $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                // Извлечение HTTP-заголовков ответа
                // Идентификатор запроса
                $requestId = preg_match('/RequestId: (\d+)/', $responseHeaders,
                  $arr) ? $arr[1] : false;
                //  Рекомендуемый интервал в секундах для проверки готовности отчета
                $retryIn = preg_match('/retryIn: (\d+)/', $responseHeaders,
                  $arr) ? $arr[1] : 60;

                if ($httpCode == 400) {

                    //                    echo "Параметры запроса указаны неверно или достигнут лимит отчетов в очереди<br>";
                    //                    echo "RequestId: {$requestId}<br>";
                    //                    echo "JSON-код запроса:<br>{$body}<br>";
                    //                    echo "JSON-код ответа сервера:<br>{$responseBody}<br>";
                    $this->apiErr = $responseBody;
                    break;

                } elseif ($httpCode == 200) {

                    //                    echo "Отчет создан успешно<br>";
                    //                    echo "RequestId: {$requestId}<br>";
                    //echo $responseBody;

                    $str = preg_replace("/Total rows:(.*)/", "", $responseBody);
                    $str = explode("\n", $str);
                    unset($str[0]);
                    unset($str[1]);

                    foreach ($str as $line) {
                        if (!empty($line)) {
                            list($date, $compaingId, $clicks, $coast) = explode("\t",
                              $line);
                            $linearray[] = [
                              "DateTime" => $date,
                              "compaingId" => $compaingId,
                              "clicks" => $clicks,
                              "coast" => $coast,
                            ];
                        }
                    };

                    if (isset($linearray) && !empty($linearray)) {
                        foreach ($linearray as $coasts) {
                            $coast += $coasts['coast'];
                        }
                    }

                    if (isset($coast) && !empty($coast)) {
                        return $coast;
                    } else {
                        $coast = 0;
                        return $coast;
                    }


                } elseif ($httpCode == 201) {

                    //                    echo "Отчет успешно поставлен в очередь в режиме офлайн<br>";
                    //                    echo "Повторная отправка запроса через {$retryIn} секунд<br>";
                    //                    echo "RequestId: {$requestId}<br>";

                    sleep($retryIn);

                } elseif ($httpCode == 202) {

                    //                    echo "Отчет формируется в режиме offline.<br>";
                    //                    echo "Повторная отправка запроса через {$retryIn} секунд<br>";
                    //                    echo "RequestId: {$requestId}<br>";

                    sleep($retryIn);

                } elseif ($httpCode == 500) {

                    //                    echo "При формировании отчета произошла ошибка. Пожалуйста, попробуйте повторить запрос позднее<br>";
                    //                    echo "RequestId: {$requestId}<br>";
                    //                    echo "JSON-код ответа сервера:<br>{$responseBody}<br>";
                    $this->apiErr = $responseBody;
                    break;

                } elseif ($httpCode == 502) {

                    //                    echo "Время формирования отчета превысило серверное ограничение.<br>";
                    //                    echo "Пожалуйста, попробуйте изменить параметры запроса - уменьшить период и количество запрашиваемых данных.<br>";
                    //                    echo "RequestId: {$requestId}<br>";
                    $this->apiErr = "Время формирования отчета превысило серверное ограничение.<br>";
                    break;

                } else {

                    //                    echo "Произошла непредвиденная ошибка.<br>";
                    //                    echo "RequestId: {$requestId}<br>";
                    //                    echo "JSON-код запроса:<br>{$body}<br>";
                    //                    echo "JSON-код ответа сервера:<br>{$responseBody}<br>";
                    $this->apiErr = $responseBody;
                    break;

                }
            }
        }

        curl_close($curl);
    }
}