<?php

namespace App\Http\Traits;

use Yandex\OAuth\OAuthClient;

trait YandexTrait
{

    public static function YandexAuthClient()
    {
        $client = new OAuthClient(env('YANDEX_CLIENT_ID'));
        // сделать редирект и выйти
        $client->authRedirect(true);
        //Передать в запросе какое-то значение в параметре state, чтобы Yandex в ответе его вернул
        $state = 'yandex-php-library';
        $client->authRedirect(true, OAuthClient::CODE_AUTH_TYPE, $state);
    }
}