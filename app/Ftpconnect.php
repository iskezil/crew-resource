<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ftpconnect extends Model
{

    public $timestamps = false;

    protected $fillable = [
      'user_id',
      'ftpname',
      'ftphost',
      'ftpuser',
      'ftppass',
      'ftpport',
      'ftpdir'
    ];
}
