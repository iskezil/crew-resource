<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metrika_status extends Model
{

    public $timestamps = false;

    protected $fillable = ['site_id', 'counter_id', 'metrika_verify_status'];
}
