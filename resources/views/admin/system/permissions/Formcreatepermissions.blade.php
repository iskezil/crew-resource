@extends('layouts.app')

@section('styles')
@endsection

@section('scripts')
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Добавление привилегии </h6>
        <div class="card-body">
            {!! Form::open(['route' => 'permissions.create']) !!}
            <div class="form-group">
                {!! Form::label('name', 'Переменная привилегии:') !!}
                {!! Form::text('name', null, ['class' =>  $errors->has('name') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('name'))
                    <small class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </small>
                @endif
            </div>

            <div class="form-group">
                {!! Form::label('display_name', 'Отображаемое имя привилегии:') !!}
                {!! Form::text('display_name', null, ['class' => $errors->has('display_name') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('display_name'))
                    <small class="invalid-feedback">
                        {{ $errors->first('display_name') }}
                    </small>
                @endif
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Описание привилегии:') !!}
                {!! Form::text('description', null, ['class' => $errors->has('description') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('description'))
                    <small class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </small>
                @endif
            </div>


            @foreach($roles as $role)
                <div class="form-group">
                    <label class="switcher switcher-success">
                        {!! Form::checkbox('role[]', $role->id, false, ['class' => 'switcher-input']) !!}
                        <span class="switcher-indicator">
                            <span class="switcher-yes">
                                <span class="ion ion-md-checkmark"></span>
                            </span>
                            <span class="switcher-no">
                                <span class="ion ion-md-close"></span>
                            </span>
                        </span>
                        {!! Form::label('description', $role->display_name, ['class' => 'switcher-label']) !!}
                    </label>
                </div>
            @endforeach

            {!! Form::submit('Сохранить', ['class' => 'btn btn-outline-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>



@endsection

