@extends('layouts.app')
@section('styles')
    <!-- Tables -->
    <link rel="stylesheet" href="{{ mix('/vendor/libs/datatables/datatables.css') }}">

    <!-- SweetAlerts -->
    <link rel="stylesheet" href="{{ mix('/vendor/libs/bootstrap-sweetalert/bootstrap-sweetalert.css') }}">
@endsection

@section('scripts')
    <!-- Tables -->
    <script src="{{ mix('/vendor/libs/datatables/datatables.js') }}"></script>
    <script src="{{ mix('/js/tables_datatables.js') }}"></script>
    <!-- SweetAlerts -->
    <script src="{{ mix('/vendor/libs/bootbox/bootbox.js') }}"></script>
    <script src="{{ mix('/vendor/libs/bootstrap-sweetalert/bootstrap-sweetalert.js') }}"></script>

    <script src="{{ mix('/js/ui_modals.js') }}"></script>
@endsection



@section('content')
    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
        <div>Привилегии</div>
        @permission('create-permissions')
        <a href="{{ URL::route('permissions.formcreate') }}" class="btn btn-sm btn-outline-success">
            <span class="ion ion-md-add"></span>&nbsp; Добавить привилегию
        </a>
        @endpermission
    </h4>


    @if ( session('status'))
        <div class="alert alert-dark-{{ session('type') }} alert-dismissible fade show">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {{ session('status') }}
        </div>
    @endif
    <div class="card">
        <h6 class="card-header fc-left">
            Все существующие привилегии </h6>

        <div class="card-datatable table-responsive">
            <table class="datatables-demo table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Переменная</th>
                    <th>Дата создания / обновления</th>
                    <th>Статус</th>
                    <th>Опции</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($permissions as $permission)
                    <tr class="odd gradeX" id="{{ $permission->id }}">
                        <td>
                            {{ $permission->display_name }}
                        </td>
                        <td>
                            {{ $permission->name }}
                        </td>
                        <td>
                            <small>@if($permission->updated_at == $permission->created_at)
                                    Создана {{ $permission->created_at }}
                                @else
                                    Обновлена {{ $permission->updated_at }}
                                @endif
                            </small>
                        </td>
                        <td>

                            @forelse ($permission->roles as $role)
                                <span class="badge badge-outline-success">Используется</span>
                                @break
                            @empty
                                <span class="badge badge-outline-danger">Не Используется</span>
                            @endforelse
                        </td>

                        <td>

                            @permission('update-permissions')
                            <a href="{{ route('permissions.formupdate', ['id' => $permission->id])  }}"
                               class="btn btn-xs btn-outline-success btn-sm"><i class="fa fa-pencil"></i> Правка
                            </a>
                            @endpermission

                            @permission('delete-permissions')
                            <input type="hidden" id="token" value="<?php echo csrf_token(); ?>">
                            <button class="btn btn-xs btn-outline-danger delete" data-element-id="{{ $permission->id }}"
                                    data-method-post="delete" onclick="return false;"><i class="fa fa-times"></i>
                                Удалить
                            </button>
                            @endpermission

                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
