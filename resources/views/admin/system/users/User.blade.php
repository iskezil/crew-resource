@extends('layouts.app')
@section('styles')
    <!-- Tables -->
    <link rel="stylesheet" href="{{ mix('/vendor/libs/datatables/datatables.css') }}">

    <!-- SweetAlerts -->
    <link rel="stylesheet" href="{{ mix('/vendor/libs/bootstrap-sweetalert/bootstrap-sweetalert.css') }}">
@endsection

@section('scripts')
    <!-- Tables -->
    <script src="{{ mix('/vendor/libs/datatables/datatables.js') }}"></script>
    <script src="{{ mix('/js/tables_datatables.js') }}"></script>
    <!-- SweetAlerts -->
    <script src="{{ mix('/vendor/libs/bootbox/bootbox.js') }}"></script>
    <script src="{{ mix('/vendor/libs/bootstrap-sweetalert/bootstrap-sweetalert.js') }}"></script>

    <script src="{{ mix('/js/ui_modals.js') }}"></script>
@endsection



@section('content')
    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
        <div>Пользователи</div>

        {{--
            TODO: Add create users
        --}}
        {{--<a href="{{ URL::route('users.formcreate') }}" class="btn btn-sm btn-outline-success">--}}
        {{--<span class="ion ion-md-add"></span>&nbsp; Добавить пользователя--}}
        {{--</a>--}}
    </h4>


    @if ( session('status'))
        <div class="alert alert-dark-{{ session('type') }} alert-dismissible fade show">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {{ session('status') }}
        </div>
    @endif
    <div class="card">
        <h6 class="card-header fc-left">
            Все существующие пользователи </h6>

        <div class="card-datatable table-responsive">
            <table class="datatables-demo table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Имя пользователя</th>
                    <th>Группа пользователя</th>
                    <th>Email</th>
                    <th>Дата создания / обновления</th>
                    <th>Опции</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($users as $user)
                    <tr class="odd gradeX" id="{{ $user->id }}">
                        <td>
                            {{ $user->name }}
                        </td>
                        <td>
                            @forelse ($user->roles as $role)
                                {{ $role->name }}
                            @empty
                                Без группы
                            @endforelse
                        </td>
                        <td>
                            {{ $user->email }}
                        </td>
                        <td>
                            <small>@if($user->updated_at == $user->created_at)
                                    Создана {{ $user->created_at }}
                                @else
                                    Обновлена {{ $user->updated_at }}
                                @endif
                            </small>
                        </td>

                        <td>

                            @permission('update-users')
                            <a href="{{ route('users.formupdate', ['id' => $user->id])  }}"
                               class="btn btn-xs btn-outline-success btn-sm"><i class="fa fa-pencil"></i> Правка
                            </a>
                            @endpermission

                            @permission('delete-users')
                            <input type="hidden" id="token" value="<?php echo csrf_token(); ?>">
                            <button class="btn btn-xs btn-outline-danger delete" data-element-id="{{ $user->id }}"
                                    data-method-post="delete" onclick="return false;"><i class="fa fa-times"></i>
                                Удалить
                            </button>
                            @endpermission

                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
