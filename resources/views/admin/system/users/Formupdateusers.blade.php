@extends('layouts.app')

@section('styles')
@endsection

@section('scripts')
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Редактирование пользователя</h6>
        <div class="card-body">
            {!! Form::open(['route' => 'users.update']) !!}

            {!! Form::hidden('usersid', $users->id) !!}

            <div class="form-group">
                {!! Form::label('username', 'Имя:') !!}
                {!! Form::text('username', $users->name, ['class' =>  $errors->has('name') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('username'))
                    <small class="invalid-feedback">
                        {{ $errors->first('username') }}
                    </small>
                @endif
            </div>

            <div class="form-group">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::text('email', $users->email, ['class' => $errors->has('display_name') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('email'))
                    <small class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </small>
                @endif
            </div>

            <div class="form-group">
                {!! Form::label('password', 'Новый пароль:') !!}
                {!! Form::text('password', null, ['class' => $errors->has('password') ? 'form-control is-invalid' : 'form-control', 'placeholder' =>'Оставьте поле пустым и пароль не изменится']) !!}
                @if ($errors->has('password'))
                    <small class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </small>
                @endif
            </div>

            <div class="form-group">
                {!! Form::label('password_confirmation', 'Повторите пароль:') !!}
                {!! Form::text('password_confirmation', null, ['class' => $errors->has('password_confirmation') ? 'form-control is-invalid' : 'form-control', 'placeholder' =>'Оставьте поле пустым и пароль не изменится']) !!}
                @if ($errors->has('password_confirmation'))
                    <small class="invalid-feedback">
                        {{ $errors->first('password_confirmation') }}
                    </small>
                @endif
            </div>


            @foreach($roles as $role)
                <div class="form-group">
                    <label class="switcher switcher-success">
                        {!! Form::checkbox('role[]', $role->id, in_array($role->id,$role_permissions) ? true : false, ['class' => 'switcher-input']) !!}
                        <span class="switcher-indicator">
                            <span class="switcher-yes">
                                <span class="ion ion-md-checkmark"></span>
                            </span>
                            <span class="switcher-no">
                                <span class="ion ion-md-close"></span>
                            </span>
                        </span>
                        {!! Form::label('role', $role->display_name, ['class' => 'switcher-label']) !!}
                    </label>
                </div>
            @endforeach

            {!! Form::submit('Сохранить', ['class' => 'btn btn-outline-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>



@endsection

