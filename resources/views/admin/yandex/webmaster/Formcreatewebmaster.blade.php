@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ mix('/vendor/libs/bootstrap-select/bootstrap-select.css') }}">
    <link rel="stylesheet" href="{{ mix('/vendor/libs/bootstrap-multiselect/bootstrap-multiselect.css') }}">
    <link rel="stylesheet" href="{{ mix('/vendor/libs/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ mix('/vendor/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
@endsection

@section('scripts')
    <script src="{{ mix('/vendor/libs/bootstrap-select/bootstrap-select.js') }}"></script>
    <script src="{{ mix('/vendor/libs/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ mix('/vendor/libs/select2/select2.js') }}"></script>
    <script src="{{ mix('/vendor/libs/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ mix('/js/forms_selects.js') }}"></script>
    <script type="text/javascript">
        function showOrHide(cb, cat) {
            cb = document.getElementById(cb);
            cat = document.getElementById(cat);
            if (cb.checked) cat.style.display = "block";
            else cat.style.display = "none";
        }

        function showOrHideRadio(cb, cat, rad) {
            cb = document.getElementById(cb);
            cat = document.getElementById(cat);
            rad = document.getElementById(rad);
            if (cb.checked) cat.style.display = "block";
            else cat.style.display = "none";
            if (rad.checked) rad.style.display = "block";
            else rad.style.display = "none";
        }
    </script>
@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Добавление нового сайта </h6>
        <div class="card-body">
            {!! Form::open(['route' => 'webmasters.formcreate']) !!}
            <div class="form-group">
                {!! Form::label('name', 'Ссылка на сайт (желательно с указанием протокола):') !!}
                {!! Form::text('name', null, ['class' =>  $errors->has('name') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('name'))
                    <small class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </small>
                @endif
            </div>

            <div class="form-group">
                <label class="switcher switcher-success">
                    {!! Form::checkbox('webmaster', 'true', false, ['class' => 'switcher-input', 'onclick' => 'showOrHide("webmaster", "site_verify");', 'id'=>'webmaster']) !!}
                    <span class="switcher-indicator">
                        <span class="switcher-yes">
                            <span class="ion ion-md-checkmark"></span>
                        </span>
                        <span class="switcher-no">
                            <span class="ion ion-md-close"></span>
                        </span>
                    </span>
                    {!! Form::label('webmaster', 'Добавить сайт в Яндекс Вебмастер', ['class' => 'switcher-label']) !!}
                </label>
            </div>

            <div class="form-group" id="site_verify"
                 style="display: {{($errors->has('ftphost') or $errors->has('ftpuser') or $errors->has('ftppass') or $errors->has('ftpdir') or $errors->has('ftpport')) ? 'block' : 'none'}}">
                <label class="switcher switcher-success">
                    {!! Form::checkbox('site_verify', 'true', false, ['class' => 'switcher-input dd-list', 'onclick' => 'showOrHide("site_verifys", "sitemap");', 'id'=>'site_verifys']) !!}
                    <span class="switcher-indicator">
                        <span class="switcher-yes">
                            <span class="ion ion-md-checkmark"></span>
                        </span>
                        <span class="switcher-no">
                            <span class="ion ion-md-close"></span>
                        </span>
                    </span>
                    {!! Form::label('site_verify', 'Начать процесс верефикации сайта?', ['class' => 'switcher-label']) !!}
                </label>
            </div>

            <div id="sitemap"
                 style="display: {{($errors->has('ftphost') or $errors->has('ftpuser') or $errors->has('ftppass') or $errors->has('ftpdir') or $errors->has('ftpport') or $errors->has('ftpname')) ? 'block' : 'none'}}">
                <div class="alert alert-warning alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    Для верификации сайта необходимо указать данные от фтп для автоматического размещения файла проверки
                    в корне сайта
                </div>
                <div class="switchers-stacked">
                    <label class="switcher switcher-success">
                        {!! Form::radio('type_ftp', 'exist_ftp', false, ['class' => 'switcher-input',  $ftpCheck == false ? 'disabled' : 'checked', 'onclick' => 'showOrHideRadio("select_exist_ftp", "exist_ftp", "new_ftp");', 'id'=>'select_exist_ftp' ]) !!}
                        <span class="switcher-indicator">
                            <span class="switcher-yes"></span>
                            <span class="switcher-no"></span>
                        </span>
                        {!! Form::label('type_ftp', 'Выбрать сохранённые данные Ftp', ['class' => 'switcher-label']) !!}
                    </label>

                    <label class="switcher switcher-success">
                        {!! Form::radio('type_ftp', 'new_ftp', false, ['class' => 'switcher-input' , $ftpCheck == false ? 'checked' : '', 'onclick' => 'showOrHideRadio("select_new_ftp", "new_ftp", "exist_ftp");', 'id'=>'select_new_ftp']) !!}
                        <span class="switcher-indicator">
                            <span class="switcher-yes"></span>
                            <span class="switcher-no"></span>
                        </span>
                        {!! Form::label('type_ftp', 'Ввести новые данные вручную', ['class' => 'switcher-label']) !!}
                    </label>
                </div>
                <div id="exist_ftp"
                     style="display: {{ ($errors->has('ftphost') or $errors->has('ftpuser') or $errors->has('ftppass') or $errors->has('ftpdir') or $errors->has('ftpport') or $errors->has('ftpname') or $ftpCheck == false) ? 'none' : 'block' }}">
                    <div class="form-group">
                        <select class="selectpicker" name="select_exist_ftp" data-style="btn-primary">
                            @forelse($ftpConnects as $ftpConnect)

                                <option value="{{$ftpConnect->id}}">{{$ftpConnect->ftpname}}</option>

                            @empty

                            @endforelse
                        </select>
                    </div>
                </div>


                <div id="new_ftp"
                     style="display: {{ ($errors->has('ftphost') or $errors->has('ftpuser') or $errors->has('ftppass') or $errors->has('ftpdir') or $errors->has('ftpport') or $errors->has('ftpname') or $ftpCheck == false) ? 'block' : 'none' }}">
                    <div class="form-group">
                        {!! Form::label('ftphost', 'Хост:') !!}
                        {!! Form::text('ftphost', null, ['class' => $errors->has('ftphost') ? 'form-control is-invalid' : 'form-control']) !!}

                        @if ($errors->has('ftphost'))
                            <small class="invalid-feedback">
                                {{ $errors->first('ftphost') }}
                            </small>
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::label('ftpuser', 'Логин:') !!}
                        {!! Form::text('ftpuser', null, ['class' => $errors->has('ftpuser') ? 'form-control is-invalid' : 'form-control']) !!}

                        @if ($errors->has('ftpuser'))
                            <small class="invalid-feedback">
                                {{ $errors->first('ftpuser') }}
                            </small>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('ftppass', 'Пароль:') !!}
                        {!! Form::text('ftppass', null, ['class' => $errors->has('ftppass') ? 'form-control is-invalid' : 'form-control']) !!}

                        @if ($errors->has('ftppass'))
                            <small class="invalid-feedback">
                                {{ $errors->first('ftppass') }}
                            </small>
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::label('ftpdir', 'Укажите полный путь до папки с сайтом (например www/site.ru то укажите www/ или если путь такой www/public_html/site.ru то укажите www/public_html/)') !!}
                        {!! Form::text('ftpdir', null, ['class' => $errors->has('ftpdir') ? 'form-control is-invalid' : 'form-control']) !!}

                        @if ($errors->has('ftpdir'))
                            <small class="invalid-feedback">
                                {{ $errors->first('ftpdir') }}
                            </small>
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::label('ftpport', 'Номер порта:') !!}
                        {!! Form::text('ftpport', null, ['class' => $errors->has('ftpport') ? 'form-control is-invalid' : 'form-control']) !!}

                        @if ($errors->has('ftpport'))
                            <small class="invalid-feedback">
                                {{ $errors->first('ftpport') }}
                            </small>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="switcher switcher-success">
                            {!! Form::checkbox('save_ftp', 'true', false, ['class' => 'switcher-input dd-list', 'onclick' => 'showOrHide("select_ftp_name", "ftp_name");', 'id'=>'select_ftp_name']) !!}
                            <span class="switcher-indicator">
                                <span class="switcher-yes">
                                    <span class="ion ion-md-checkmark"></span>
                                </span>
                                <span class="switcher-no">
                                    <span class="ion ion-md-close"></span>
                                </span>
                            </span>
                            {!! Form::label('save_ftp', 'Сохранить данные от FTP на будущее?', ['class' => 'switcher-label']) !!}
                        </label>
                    </div>

                    <div class="form-group" id="ftp_name"
                         style="display: {{ ($errors->has('ftpname')) ? 'block' : 'none'}}">
                        {!! Form::label('ftpname', 'Укажите имя сохраняемой конфигурации:') !!}
                        {!! Form::text('ftpname', null, ['class' => $errors->has('ftpname') ? 'form-control is-invalid' : 'form-control']) !!}

                        @if ($errors->has('ftpname'))
                            <small class="invalid-feedback">
                                {{ $errors->first('ftpname') }}
                            </small>
                        @endif
                    </div>
                </div>


                <div class="form-group">
                    <label class="switcher switcher-success">
                        {!! Form::checkbox('sitemap', 'true', false, ['class' => 'switcher-input dd-list', 'onclick' => 'showOrHide("sitemaps", "sitemoption");', 'id'=>'sitemaps']) !!}
                        <span class="switcher-indicator">
                            <span class="switcher-yes">
                                <span class="ion ion-md-checkmark"></span>
                            </span>
                            <span class="switcher-no">
                                <span class="ion ion-md-close"></span>
                            </span>
                        </span>
                        {!! Form::label('sitemap', 'Добавить файл Sitemap?', ['class' => 'switcher-label']) !!}
                    </label>
                </div>
            </div>


            <div id="sitemoption"
                 style="display: {{($errors->has('dbhost') or $errors->has('dbuser') or $errors->has('dbpass') or $errors->has('dbname') or $errors->has('dbport') or $errors->has('sitemapurl')) ? 'block' : 'none'}}">
                <div class="form-group">
                    <label class="switcher switcher-success">
                        {!! Form::checkbox('sitemapauto', 'true', false, ['class' => 'switcher-input dd-list', 'onclick' => 'showOrHide("sitemapauto", "sitemapdbform");', 'id'=>'sitemapauto']) !!}
                        <span class="switcher-indicator">
                            <span class="switcher-yes">
                                <span class="ion ion-md-checkmark"></span>
                            </span>
                            <span class="switcher-no">
                                <span class="ion ion-md-close"></span>
                            </span>
                        </span>
                        {!! Form::label('sitemapauto', 'Определить ссылку на сайтмап автоматически', ['class' => 'switcher-label']) !!}
                    </label>
                </div>

                <div id="sitemapdbform"
                     style="display: {{($errors->has('dbhost') or $errors->has('dbuser') or $errors->has('dbpass') or $errors->has('dbname') or $errors->has('dbport')) ? 'block' : 'none'}}">

                    <div class="alert alert-warning alert-dismissible fade show">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        Для определения ссылки на Sitemap в автоматическом режиме требуется подключение к базе данных.
                    </div>

                    <div class="form-group">
                        {!! Form::label('dbhost', 'Хост:') !!}
                        {!! Form::text('dbhost', null, ['class' => $errors->has('dbhost') ? 'form-control is-invalid' : 'form-control']) !!}

                        @if ($errors->has('dbhost'))
                            <small class="invalid-feedback">
                                {{ $errors->first('dbhost') }}
                            </small>
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::label('dbuser', 'Логин:') !!}
                        {!! Form::text('dbuser', null, ['class' => $errors->has('dbuser') ? 'form-control is-invalid' : 'form-control']) !!}

                        @if ($errors->has('dbuser'))
                            <small class="invalid-feedback">
                                {{ $errors->first('dbuser') }}
                            </small>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('dbpass', 'Пароль:') !!}
                        {!! Form::text('dbpass', null, ['class' => $errors->has('dbpass') ? 'form-control is-invalid' : 'form-control']) !!}

                        @if ($errors->has('dbpass'))
                            <small class="invalid-feedback">
                                {{ $errors->first('dbpass') }}
                            </small>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('dbname', 'Имя базы:') !!}
                        {!! Form::text('dbname', null, ['class' => $errors->has('dbname') ? 'form-control is-invalid' : 'form-control']) !!}

                        @if ($errors->has('dbname'))
                            <small class="invalid-feedback">
                                {{ $errors->first('dbname') }}
                            </small>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('dbport', 'Порт:') !!}
                        {!! Form::text('dbport', null, ['class' => $errors->has('dbport') ? 'form-control is-invalid' : 'form-control']) !!}

                        @if ($errors->has('dbport'))
                            <small class="invalid-feedback">
                                {{ $errors->first('dbport') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="switcher switcher-success">
                        {!! Form::checkbox('sitemaplink', 'true', false, ['class' => 'switcher-input dd-list', 'onclick' => 'showOrHide("sitemaplink", "sitemapurlform");', 'id'=>'sitemaplink']) !!}
                        <span class="switcher-indicator">
                            <span class="switcher-yes">
                                <span class="ion ion-md-checkmark"></span>
                            </span>
                            <span class="switcher-no">
                                <span class="ion ion-md-close"></span>
                            </span>
                        </span>
                        {!! Form::label('sitemaplink', 'Указать ссылку на Sitemap вручную', ['class' => 'switcher-label']) !!}
                    </label>
                </div>

                <div class="form-group" id="sitemapurlform"
                     style="display: {{$errors->has('sitemapurl') ? 'block' : 'none'}}">
                {!! Form::label('sitemapurl', 'Ссылка на файл Sitemap:') !!}
                {!! Form::text('sitemapurl', null, ['class' => $errors->has('sitemapurl') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('sitemapurl'))
                    <small class="invalid-feedback">
                        {{ $errors->first('sitemapurl') }}
                    </small>
                @endif
            </div>
            </div>


            <div class="form-group">
                <label class="switcher switcher-success">
                    {!! Form::checkbox('metrika', 'true', false, ['class' => 'switcher-input']) !!}
                    <span class="switcher-indicator">
                        <span class="switcher-yes">
                            <span class="ion ion-md-checkmark"></span>
                        </span>
                        <span class="switcher-no">
                            <span class="ion ion-md-close"></span>
                        </span>
                    </span>
                    {!! Form::label('metrika', 'Добавить сайт в Яндекс Метрику', ['class' => 'switcher-label']) !!}
                </label>
                @if ($errors->has('webmaster') or $errors->has('metrika'))
                    <small class="invalid-feedback" style="display: block">
                        Выберите хотя бы один пункт
                    </small>
                @endif
            </div>


            {!! Form::submit('Сохранить', ['class' => 'btn btn-outline-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>



@endsection

