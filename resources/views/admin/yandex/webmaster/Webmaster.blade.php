@extends('layouts.app')
@section('styles')
    <!-- Tables -->
    <link rel="stylesheet" href="{{ mix('/vendor/libs/datatables/datatables.css') }}">

    <!-- SweetAlerts -->
    <link rel="stylesheet" href="{{ mix('/vendor/libs/bootstrap-sweetalert/bootstrap-sweetalert.css') }}">
@endsection

@section('scripts')
    <!-- Tables -->
    <script src="{{ mix('/vendor/libs/datatables/datatables.js') }}"></script>
    <script src="{{ mix('/js/tables_datatables.js') }}"></script>
    <!-- SweetAlerts -->
    <script src="{{ mix('/vendor/libs/bootbox/bootbox.js') }}"></script>
    <script src="{{ mix('/vendor/libs/bootstrap-sweetalert/bootstrap-sweetalert.js') }}"></script>

    <script src="{{ mix('/js/ui_modals.js') }}"></script>
@endsection



@section('content')
    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
        <div>Сайты в Yandex Webmaster</div>
        <a href="{{ URL::route('webmasters.formcreate') }}" class="btn btn-sm btn-outline-success">
            <span class="ion ion-md-add"></span>&nbsp; Добавить сайт
        </a>
    </h4>


    @if ( session('success'))
        <div class="alert alert-dark-success alert-dismissible fade show">
            <button type="button" class="close" data-dismiss="alert">×</button>
            @foreach(session('success') as $warning)
                {{ $warning }}<br>
            @endforeach
        </div>
    @endif
    @if ( session('warning'))
        <div class="alert alert-dark-warning alert-dismissible fade show">
            <button type="button" class="close" data-dismiss="alert">×</button>
            @foreach(session('warning') as $warning)
                {{ $warning }}<br>
            @endforeach
        </div>
    @endif
    @if ( session('error'))
        <div class="alert alert-dark-danger alert-dismissible fade show">
            <button type="button" class="close" data-dismiss="alert">×</button>
            @foreach(session('error') as $warning)
                {{ $warning }}<br>
            @endforeach
        </div>
    @endif
    <div class="card">
        <h6 class="card-header fc-left">
            Все существующие роли </h6>

        <div class="card-datatable table-responsive">
            <table class="datatables-demo table table-striped table-bordered">
                <thead>
                <tr>
                    <th>id</th>
                    <th>user_id</th>
                    <th>name</th>
                    <th>Дата создания / обновления</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($sites as $site)
                    <tr class="odd gradeX" id="{{ $site->id }}">
                        <td>
                            {{ $site->id }}
                        </td>
                        <td>
                            {{ $site->user_id }}
                        </td>
                        <td>
                            {{ $site->name }}
                        </td>
                        <td>
                            <small>@if($site->updated_at == $site->created_at)
                                    Создана {{ $site->created_at }}
                                @else
                                    Обновлена {{ $site->updated_at }}
                                @endif
                            </small>
                        </td>

                        <td>

                            @permission('update-roles')
                            <a href="{{ route('roles.formupdate', ['id' => $site->id])  }}"
                               class="btn btn-xs btn-outline-success btn-sm"><i class="fa fa-pencil"></i> Правка
                            </a>
                            @endpermission

                            @permission('delete-roles')
                            <input type="hidden" id="token" value="<?php echo csrf_token(); ?>">
                            <button class="btn btn-xs btn-outline-danger delete" data-element-id="{{ $site->id }}"
                                    data-method-post="delete" onclick="return false;"><i class="fa fa-times"></i>
                                Удалить
                            </button>
                            @endpermission

                        </td>

                    </tr>

                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection
