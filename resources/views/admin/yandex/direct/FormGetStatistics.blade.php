@extends('layouts.app')

@section('styles')

    <link rel="stylesheet" href="{{ mix('vendor/libs/bootstrap-datepicker/bootstrap-datepicker.css') }}">
@endsection

@section('scripts')
    <script src="{{ mix('vendor/libs/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ mix('js/forms_pickers.js') }}"></script>
@endsection

@section('content')
    <div class="card mb-4">

        <h6 class="card-header">
            Получение статистики </h6>
        <div class="card-body">
            @if ( session('status'))
                <div class="alert alert-dark-{{session('type')}} alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    {!! session('status') !!}<br>
                </div>
            @endif

            @if( session('error'))
                <div class="alert alert-dark-warning alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    {!! session('error') !!}
                </div>
            @endif

            {!! Form::open(['route' => 'directs.getstatistic']) !!}
            <div class="input-daterange input-group" id="datepicker-range">
                {!! Form::text('start', null, ['class' =>  'form-control', 'autocomplete' => 'off']) !!}
                <div class="input-group-prepend">
                    <span class="input-group-text">по</span>
                </div>
                {!! Form::text('end', null, ['class' =>  'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <br>
            {!! Form::submit('Запросить статистику', ['class' => 'btn btn-outline-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>



@endsection

