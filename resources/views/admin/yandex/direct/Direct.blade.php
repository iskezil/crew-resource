@extends('layouts.app')
@section('styles')
    <script src="{{ mix('/js/pages_projects_list.js') }}"></script>

    <!-- SweetAlerts -->
    <link rel="stylesheet" href="{{ mix('/vendor/libs/bootstrap-sweetalert/bootstrap-sweetalert.css') }}">
@endsection

@section('scripts')

    <!-- SweetAlerts -->
    <script src="{{ mix('/vendor/libs/bootbox/bootbox.js') }}"></script>
    <script src="{{ mix('/vendor/libs/bootstrap-sweetalert/bootstrap-sweetalert.js') }}"></script>
    <script src="{{ mix('/js/ui_modals.js') }}"></script>
@endsection



@section('content')


    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
        <div>Акаунты в Yandex Direct</div>
        <div>
            <a href="{{ URL::route('directs.getarchive') }}" class="btn btn-sm btn-outline-danger">
                <span class="ion ion-ios-trash"></span>&nbsp; Архив
            </a>
            <a href="{{ URL::route('directs.statistics') }}" class="btn btn-sm btn-outline-primary">
                <span class="ion ion-ios-trending-up"></span>&nbsp; Статистика
            </a>
            <a href="{{ URL::route('directs.formcreate') }}" class="btn btn-sm btn-outline-success">
                <span class="ion ion-md-add"></span>&nbsp; Добавить аккаунт
            </a>
        </div>

    </h4>


    @if( session('status'))
        <div class="alert alert-dark-{{session('type')}} alert-dismissible fade show">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {{ session('status') }}<br>
        </div>
    @endif






    <div class="row">

        @foreach ($userDirects as $userDirect)
            <div class="col-sm-6 col-xl-12" id="{{ $userDirect->id }}">
                <div class="card mb-4 border-{{ $campaignStatus[$userDirect->user_login] }}">

                    <div class="card-body d-flex justify-content-between align-items-start pb-3">
                        <div>
                            <a href="#company-faq-{{ $userDirect->id }}"
                               class="text-dark text-big font-weight-semibold collapsed" data-toggle="collapse"
                               aria-expanded="false">{{ $userDirect->token_name }}</a>
                            <span class="badge badge-{{ $campaignStatus[$userDirect->user_login] }} align-text-bottom ml-1">{{ $campaignStatus[$userDirect->user_login] == 'success' ? 'Всё хорошо' : 'Аларм блять'}}</span>
                            <span class="badge badge-{{ $campaignStatus[$userDirect->user_login] }} align-text-bottom ml-1">{{ $money[$userDirect->user_login] > 0 ? 'Осталось: ' . $money[$userDirect->user_login] . ' Руб.' : 'Осталось: ' . $money[$userDirect->user_login] . ' Руб.'}}</span>
                            <div class="text-muted small mt-1">{{$userDirect->user_login}}@yandex.ru</div>
                        </div>
                        <div class="btn-group project-actions">
                            <button type="button"
                                    class="btn btn-sm btn-default icon-btn borderless btn-round md-btn-flat dropdown-toggle hide-arrow"
                                    data-toggle="dropdown">
                                <i class="ion ion-ios-more"></i>
                            </button>

                            <div class="dropdown-menu dropdown-menu-right">
                                <input type="hidden" id="token" value="<?php echo csrf_token(); ?>">
                                <button class="dropdown-item delete" data-element-id="{{ $userDirect->id }}"
                                        data-method-post="archive"
                                        data-title-post="Вы точно хотите архивировать аккаунт?" data-text-post=" "
                                        onclick="return false;">Архивировать
                                </button>

                                {!! Form::open(['route' => 'directs.refresh.token']) !!}

                                <div class="form-group">
                                    {!! Form::hidden('token_id', $userDirect->id) !!}
                                </div>
                                {!! Form::submit('Обновить токен', ['class' => 'dropdown-item']) !!}

                                {!! Form::close() !!}

                                {{--Форма перезагрузки Р.К.--}}
                                {!! Form::open(['route' => 'directs.refresh']) !!}

                                <div class="form-group">
                                    {!! Form::hidden('step_one', '1') !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::hidden('token_id', $userDirect->id) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::hidden('token_name', $userDirect->token_name) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::hidden('token', $userDirect->token) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::hidden('user_login', $userDirect->user_login) !!}
                                </div>

                                {!! Form::submit('Перезапустить', ['class' => 'dropdown-item']) !!}

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <div class="progress rounded-0" style="height: 3px;">
                        <div class="progress-bar @if($money[$userDirect->user_login] == 0) bg-danger @elseif($money[$userDirect->user_login] < 600) bg-warning @else bg-success @endif"
                             style="width: {{100 - $money[$userDirect->user_login] / 12000 * 100}}%;"></div>
                    </div>
                    <div class="card-body small pt-2 pb-0">
                        Бюджет израсходаван на <strong>{{round(100 - $money[$userDirect->user_login] / 12000 * 100)}}
                            %</strong>
                    </div>
                    <div class="card-body pb-3 collapse" id="company-faq-{{ $userDirect->id }}">
                        <ul class="list-group">
                            @forelse($Campaigns[$userDirect->user_login] as $Campaign)
                                <li class="list-group-item d-flex justify-content-between align-items-center">{{ $Campaign->Name }}
                                    <span class="badge badge-{{ $Campaign->StatusClarification == 'Идут показы' ? 'success' : 'danger' }}">{{$Campaign->StatusClarification}}</span>
                                </li>
                            @empty
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Неизвестная ошибка
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection