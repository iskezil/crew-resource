@extends('layouts.app')

@section('styles')

@endsection

@section('scripts')

@endsection

@section('content')
    <div class="card mb-4">

        <h6 class="card-header">
            Добавление нового аккаунта </h6>
        <div class="card-body">
            @if ( session('status'))
                <div class="alert alert-dark-{{session('type')}} alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    {{ session('status') }}<br>
                </div>
            @endif
            {!! Form::open(['route' => 'directs.formcreate']) !!}
            <div class="form-group">
                {!! Form::label('token_name', 'Название аккаунта:') !!}
                {!! Form::text('token_name', null, ['class' =>  $errors->has('token_name') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('token_name'))
                    <small class="invalid-feedback">
                        {{ $errors->first('token_name') }}
                    </small>
                @endif
            </div>
            <div class="form-group">
                {!! Form::label('user_login', 'Логин пользователя:') !!}
                {!! Form::text('user_login', null, ['class' =>  $errors->has('user_login') ? 'form-control is-invalid' : 'form-control']) !!}
                @if ($errors->has('user_login'))
                    <small class="invalid-feedback">
                        {{ $errors->first('user_login') }}
                    </small>
                @endif
            </div>

            {!! Form::submit('Сохранить', ['class' => 'btn btn-outline-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>



@endsection

