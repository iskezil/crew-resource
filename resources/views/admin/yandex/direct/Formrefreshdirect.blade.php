@extends('layouts.app')

@section('styles')

@endsection

@section('scripts')

@endsection

@section('content')
    <div class="card mb-4">
        <h6 class="card-header">
            Перенос рекламы на новый аккаунт </h6>
        <div class="card-body">

            @if( session('new_token_id'))
                <div class="alert alert-dark-danger alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    На данном этапе всё готово к началу переноса кампаний.<br>
                    Внимательно проверьте данные:<br>
                    Рекламная капмания переносится с аккаунта: {{ session('old_user_name') }}@yandex.ru<br>
                    В аккаунт: {{ session('new_user_login') }}@yandex.ru<br>
                </div>

                {!! Form::open(['route' => 'directs.startrefresh']) !!}


                <div class="form-group">
                    {!! Form::hidden('old_token_id', session('old_token_id')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('site_link', 'Ссылка на сайт (с указанием протокола):') !!}
                    {!! Form::text('site_link', null, ['class' =>  $errors->has('site_link') ? 'form-control is-invalid' : 'form-control']) !!}
                    @if ($errors->has('site_link'))
                        <small class="invalid-feedback">
                            {{ $errors->first('site_link') }}
                        </small>
                    @endif
                </div>

                <div class="form-group">
                    {!! Form::hidden('new_token_id', session('new_token_id')) !!}
                </div>

                {!! Form::submit('Перенести кампанию.', ['class' => 'btn btn-outline-success']) !!}

                {!! Form::close() !!}
            @else
                <div class="alert alert-dark-primary alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    Внимание! На данном этапе вы должны быть авторизованы под новым пользователем в вашем яндексе.
                </div>

                {!! Form::open(['route' => 'directs.refresh']) !!}
                <div class="form-group">
                    {!! Form::label('new_user_login', 'Логин нового пользователя:') !!}
                    {!! Form::text('new_user_login', null, ['class' =>  $errors->has('new_user_login') ? 'form-control is-invalid' : 'form-control']) !!}
                    @if ($errors->has('new_user_login'))
                        <small class="invalid-feedback">
                            {{ $errors->first('new_user_login') }}
                        </small>
                    @endif
                </div>

                <div class="form-group">
                    {!! Form::hidden('step_two', '2') !!}
                </div>
                <div class="form-group">
                    {!! Form::hidden('old_token_id', $userdata['token_id']) !!}
                </div>
                <div class="form-group">
                    {!! Form::hidden('old_token_name', $userdata['token_name']) !!}
                </div>
                <div class="form-group">
                    {!! Form::hidden('old_token', $userdata['token']) !!}
                </div>
                <div class="form-group">
                    {!! Form::hidden('old_user_login', $userdata['user_login']) !!}
                </div>

                {!! Form::submit('Начать процесс переноса', ['class' => 'btn btn-outline-success']) !!}

                {!! Form::close() !!}
            @endif
        </div>
    </div>



@endsection

