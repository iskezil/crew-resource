@extends('layouts.app')
@section('styles')

@endsection

@section('scripts')

@endsection
@section('content')
    <script>
        $(document).ready(function () {
            $('.delete').click(function () {
                var method = $(this);
                var element_id = method.attr('data-element-id');
                var form = document.getElementById(element_id);
                form.submit();
            });

        });

    </script>
@endsection
