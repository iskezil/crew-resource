<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
  'middleware' => 'auth',
  'namespace' => 'Admin',
  'prefix' => 'system/role',
  'as' => 'roles.',
], function () {
    Route::get('/', ['as' => 'show', 'uses' => 'AdminRoleController@index']);

    Route::get('/create',
      ['as' => 'formcreate', 'uses' => 'AdminRoleController@showformcreate']);

    Route::post('/create',
      ['as' => 'create', 'uses' => 'AdminRoleController@create']);

    Route::get('/update/{id}',
      ['as' => 'formupdate', 'uses' => 'AdminRoleController@showformupdate']);

    Route::post('/update',
      ['as' => 'update', 'uses' => 'AdminRoleController@update']);

    Route::post('/delete',
      ['as' => 'delete', 'uses' => 'AdminRoleController@delete']);
});

Route::group([
  'middleware' => 'auth',
  'namespace' => 'Admin',
  'prefix' => 'system/permission',
  'as' => 'permissions.',
], function () {
    Route::get('/',
      ['as' => 'show', 'uses' => 'AdminPermissionController@index']);

    Route::get('/create',
      [
        'as' => 'formcreate',
        'uses' => 'AdminPermissionController@showformcreate',
      ]);

    Route::post('/create',
      ['as' => 'create', 'uses' => 'AdminPermissionController@create']);

    Route::get('/update/{id}',
      [
        'as' => 'formupdate',
        'uses' => 'AdminPermissionController@showformupdate',
      ]);

    Route::post('/update',
      ['as' => 'update', 'uses' => 'AdminPermissionController@update']);

    Route::post('/delete',
      ['as' => 'delete', 'uses' => 'AdminPermissionController@delete']);
});

Route::group([
  'middleware' => 'auth',
  'namespace' => 'Admin',
  'prefix' => 'system/user',
  'as' => 'users.',
], function () {
    Route::get('/',
      ['as' => 'show', 'uses' => 'AdminUserController@index']);

    //    Route::get('/create',
    //      [
    //        'as' => 'formcreate',
    //        'uses' => 'AdminUserController@showformcreate',
    //      ]);
    //
    //    Route::post('/create',
    //      ['as' => 'create', 'uses' => 'AdminUserController@create']);

    Route::get('/update/{id}',
      [
        'as' => 'formupdate',
        'uses' => 'AdminUserController@showformupdate',
      ]);

    Route::post('/update',
      ['as' => 'update', 'uses' => 'AdminUserController@update']);

    Route::post('/delete',
      ['as' => 'delete', 'uses' => 'AdminUserController@delete']);
});

Route::group([
  'middleware' => 'auth',
  'namespace' => 'Admin',
  'prefix' => 'yandex/webmaster',
  'as' => 'webmasters.',
], function () {
    Route::get('/',
      ['as' => 'show', 'uses' => 'AdminYandexWebmasterController@index']);

    Route::get('/create',
      [
        'as' => 'formcreate',
        'uses' => 'AdminYandexWebmasterController@showformcreate',
      ]);

    Route::post('/create',
      ['as' => 'create', 'uses' => 'AdminYandexWebmasterController@create']);


    //    Route::get('/update/{id}',
    //      [
    //        'as' => 'formupdate',
    //        'uses' => 'AdminUserController@showformupdate',
    //      ]);
    //
    //    Route::post('/update',
    //      ['as' => 'update', 'uses' => 'AdminUserController@update']);
    //
    Route::post('/delete',
      ['as' => 'delete', 'uses' => 'AdminYandexWebmasterController@delete']);

    Route::get('/callback',
      [
        'as' => 'callback',
        'uses' => 'AdminYandexWebmasterController@callback',
      ]);
});
Route::group([
  'middleware' => 'auth',
  'as' => 'home',
], function () {
    Route::get('/', 'HomeController@index')->name('home');
});

Route::group([
  'middleware' => 'auth',
  'namespace' => 'Admin',
  'prefix' => 'yandex/direct',
  'as' => 'directs.',
], function () {
    Route::get('/',
      ['as' => 'show', 'uses' => 'AdminYandexDirectController@index']);

    Route::get('/create',
      [
        'as' => 'formcreate',
        'uses' => 'AdminYandexDirectController@showformcreate',
      ]);

    Route::post('/create',
      ['as' => 'create', 'uses' => 'AdminYandexDirectController@create']);

    Route::post('/refresh',
      [
        'as' => 'refresh',
        'uses' => 'AdminYandexDirectController@showformrefresh',
      ]);

    Route::post('/refresh-token',
      [
        'as' => 'refresh.token',
        'uses' => 'AdminYandexDirectController@refreshToken',
      ]);

    Route::get('/check-refresh',
      [
        'as' => 'check.refresh',
        'uses' => 'AdminYandexDirectController@checkshowformrefresh',
      ]);

    Route::post('/startrefresh',
      [
        'as' => 'startrefresh',
        'uses' => 'AdminYandexDirectController@refresh',
      ]);
    //    Route::get('/update/{id}',
    //      [
    //        'as' => 'formupdate',
    //        'uses' => 'AdminUserController@showformupdate',
    //      ]);
    //
    //    Route::post('/update',
    //      ['as' => 'update', 'uses' => 'AdminUserController@update']);
    //
    Route::post('/archive',
      ['as' => 'archive', 'uses' => 'AdminYandexDirectController@archive']);

    Route::get('/getarchive',
      ['as' => 'getarchive', 'uses' => 'AdminYandexDirectController@index']);

    Route::get('/statistics',
      [
        'as' => 'statistics',
        'uses' => 'AdminYandexDirectController@statistics',
      ]);

    Route::post('/getstatistic',
      [
        'as' => 'getstatistic',
        'uses' => 'AdminYandexDirectController@getstatistic',
      ]);

    Route::get('/callback',
      [
        'as' => 'callback',
        'uses' => 'AdminYandexDirectController@callback',
      ]);
});



Auth::routes();